# DOCKER-VERSION 0.3.4
# sshd, openjpeg2, openslide, iipsrv, apache
#
# VERSION               0.2

# this version provides 
#	modified cytomine iipsrv with
#		fixed cache key hash. since v0.1.1
#		openslide
#			calc best layer myself to avoid 4.000004 vs 4. since v0.1.1
#			fast ABGR to RGB.  since v0.1.1
#			fast area averaging for virtual tiles (instead of nearest neighbor). since v0.2
#			internal tile caching. since v0.2

FROM     ubuntu:14.04
MAINTAINER Tony Pan "tcp1975@gmail.com"

# build with
#  sudo docker build --rm=true -t="repo/imgname" .

### update
RUN apt-get -q update
RUN apt-get -q -y upgrade
RUN apt-get -q -y dist-upgrade
RUN apt-get -q -y install openssh-server

### need build tools for building openslide and later iipsrv
RUN apt-get -q -y install git autoconf automake make libtool pkg-config cmake

RUN mkdir /root/src

### install apache and dependencies. using fcgid
RUN apt-get -q -y install apache2 libapache2-mod-fcgid libfcgi0ldbl
RUN a2enmod rewrite
RUN a2enmod fcgid

## get our configuration files
WORKDIR /root/src
RUN git clone https://tcpan@bitbucket.org/tcpan/iip-openslide-docker.git

## replace apache's default fcgi config with ours.
RUN rm /etc/apache2/mods-enabled/fcgid.conf
RUN ln -s /root/src/iip-openslide-docker/apache2-iipsrv-fcgid.conf /etc/apache2/mods-enabled/fcgid.conf



### install iipmooviewer 2.0 beta
#WORKDIR /root/src
#RUN wget http://merovingio.c2rmf.cnrs.fr/iipimage/iipmooviewer-2.0-beta.tar.gz
#RUN tar xvfz iipmooviewer-2.0-beta.tar.gz
#RUN mkdir /var/www/html/iipmooviewer
#RUN mv /root/src/iipmooviewer-2.0-beta /var/www/html/iipmooviewer
#RUN cp /root/src/iip-openslide-docker/iipmooviewer/index.html /var/www/html/iipmooviewer/index.html

### install openseadragon 1.0.0  - 1.1 does not work
#WORKDIR /root/src/iip-openslide-docker
#RUN tar xvfz openseadragon-bin-1.0.0.tar.gz
#RUN mkdir /var/www/html/openseadragon
#RUN mv /root/src/iip-openslide-docker/openseadragon-bin-1.0.0 /var/www/html/openseadragon
#RUN cp /root/src/iip-openslide-docker/openseadragon/index.html /var/www/html/openseadragon/index.html

### install iipzoom 0.2
#WORKDIR /root/src/iip-openslide-docker
#RUN mkdir /var/www/html/iipzoom
#RUN cp -r /root/src/iip-openslide-docker/iipzoom-0.2 /var/www/html/iipzoom

# now accessible via http://ipaddress/iipmooviewer/index.html?image=xxx,
#		 http://ipaddress/openseadragon/index.html?image=xxx
#		 http://ipaddress/iipzoom/index.html?image=xxx
# symlink to original directory does NOT work.

## expose some ports
EXPOSE 80
#EXPOSE 443

## setup a mount point for images.  - this is external to the docker container.
RUN mkdir -p /mnt/images

### set up the ssh daemon
RUN mkdir /var/run/sshd
RUN echo 'root:iipdocker' | chpasswd
RUN sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config
## expose some ports
EXPOSE 22


### prereqs for openslide
RUN apt-get -q -y install zlib1g-dev libpng12-dev libjpeg-dev libtiff5-dev libgdk-pixbuf2.0-dev libxml2-dev libsqlite3-dev libcairo2-dev libglib2.0-dev

WORKDIR /root/src

### openjpeg version in ubuntu 14.04 is 1.3, too old and does not have openslide required chroma subsampled images support.  download 2.1.0 from source and build
RUN wget http://sourceforge.net/projects/openjpeg.mirror/files/2.1.0/openjpeg-2.1.0.tar.gz
RUN tar xvfz openjpeg-2.1.0.tar.gz
RUN mkdir /root/src/openjpeg-bin
WORKDIR /root/src/openjpeg-bin
RUN cmake -DBUILD_JPIP=ON -DBUILD_SHARED_LIBS=ON -DCMAKE_BUILD_TYPE=Release -DBUILD_CODEC=ON -DBUILD_PKGCONFIG_FILES=ON /root/src/openjpeg-2.1.0
RUN make
RUN make install

### Openslide
WORKDIR /root/src
## get the openslide source code
RUN git clone https://github.com/openslide/openslide.git

## build openslide 
WORKDIR /root/src/openslide
RUN autoreconf -i 
#RUN ./configure --enable-static --enable-shared=no
# may need to set OPENJPEG_CFLAGS='-I/usr/local/include' and OPENJPEG_LIBS='-L/usr/local/lib -lopenjp2'
# and the corresponding TIFF flags and libs to where bigtiff lib is installed.
RUN ./configure
RUN make
RUN make install

###  iipsrv
WORKDIR /root/src
RUN apt-get -q -y install g++ libmemcached-dev libjpeg-turbo8-dev
## get my fork from cyctomine repo
RUN git clone https://bitbucket.org/tcpan/iipsrv-openslide.git iipsrv

## build iipsrv
WORKDIR /root/src/iipsrv
RUN git checkout tags/v0.2
RUN ./autogen.sh
#RUN ./configure --enable-static --enable-shared=no
RUN ./configure
RUN make

## create a directory for iipsrv's fcgi binary
RUN mkdir -p /var/www/localhost/fcgi-bin/
RUN cp /root/src/iipsrv/src/iipsrv.fcgi /var/www/localhost/fcgi-bin/


## run the script to start apache and sshd and keep the container running.
# use "service apache2 start"
CMD ["/usr/sbin/sshd", "-D"]

