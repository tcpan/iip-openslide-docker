/**
 * @file    cpucheck.h
 * @ingroup
 * @author  tpan
 * @brief
 * @details
 *
 * Copyright (c) 2014 Georgia Institute of Technology.  All Rights Reserved.
 *
 * TODO add License
 */
#ifndef CPUCHECK_H_
#define CPUCHECK_H_

#include <immintrin.h>

namespace utils {

  namespace hw {

    // functions to check for CPU capabilities
#ifdef _WIN32

    //  Windows
#define cpuid(info,x)    __cpuidex(info,x,0)

#else

    //  GCC Inline Assembly
    static inline void cpuid(int CPUInfo[4],int InfoType){
      __asm__ __volatile__ (
          "cpuid":
                            "=a" (CPUInfo[0]),
                            "=b" (CPUInfo[1]),
                            "=c" (CPUInfo[2]),
                            "=d" (CPUInfo[3]) :
                            "a" (InfoType), "c" (0)
      );
    }


#if __GNUC__ > 4 || __GNUC__ == 4 && __GNUC_MINOR__ >= 4
    static inline unsigned long long _xgetbv(unsigned int index){
      unsigned int eax, edx;
      __asm__ __volatile__(
          "xgetbv" :
                           "=a"(eax),
                           "=d"(edx) :
                           "c"(index)
                           );
      return ((unsigned long long)edx << 32) | eax;
    }
#else
#define _xgetbv() 0
#endif

#endif


    //  Misc.
    bool HW_MMX;
    bool HW_x64;
    bool HW_ABM;      // Advanced Bit Manipulation
    bool HW_RDRAND;
    bool HW_BMI1;
    bool HW_BMI2;
    bool HW_ADX;

    //  SIMD: 128-bit
    bool HW_SSE;
    bool HW_SSE2;
    bool HW_SSE3;
    bool HW_SSSE3;
    bool HW_SSE41;
    bool HW_SSE42;
    bool HW_SSE4a;
    bool HW_AES;
    bool HW_SHA;

    //  SIMD: 256-bit
    bool HW_XSAVE_XRSTORE;
    bool HW_AVX;
    bool OS_AVX;
    bool HW_XOP;
    bool HW_FMA3;
    bool HW_FMA4;
    bool HW_AVX2;

    //  SIMD: 512-bit
    bool HW_AVX512F;   //  AVX512 Foundation
    bool HW_AVX512CD;  //  AVX512 Conflict Detection
    bool HW_AVX512PF;  //  AVX512 Prefetch
    bool HW_AVX512ER;  //  AVX512 Exponential + Reciprocal
    bool HW_AVX512VL;  //  AVX512 Vector Length Extensions
    bool HW_AVX512BW;  //  AVX512 Byte + Word
    bool HW_AVX512DQ;  //  AVX512 Doubleword + Quadword


    void checkCPUCapability() {
      int info[4];
      // standard features
      cpuid(info, 0);
      int nIds = info[0];

      // extended features
      cpuid(info, 0x80000000);
      unsigned nExIds = info[0];

      //  Detect Features
      if (nIds >= 0x00000001){
        cpuid(info,0x00000001);
        HW_MMX    = (info[3] & ((int)1 << 23)) != 0;
        HW_SSE    = (info[3] & ((int)1 << 25)) != 0;
        HW_SSE2   = (info[3] & ((int)1 << 26)) != 0;
        HW_SSE3   = (info[2] & ((int)1 <<  0)) != 0;

        HW_SSSE3  = (info[2] & ((int)1 <<  9)) != 0;
        HW_SSE41  = (info[2] & ((int)1 << 19)) != 0;
        HW_SSE42  = (info[2] & ((int)1 << 20)) != 0;
        HW_AES    = (info[2] & ((int)1 << 25)) != 0;

        HW_XSAVE_XRSTORE =
                    (info[2] & ((int)1 << 27)) != 0;
        HW_AVX    = (info[2] & ((int)1 << 28)) != 0;
        HW_FMA3   = (info[2] & ((int)1 << 12)) != 0;

        HW_RDRAND = (info[2] & ((int)1 << 30)) != 0;
      }
      if (nIds >= 0x00000007){
        cpuid(info,0x00000007);
        HW_AVX2   = (info[1] & ((int)1 <<  5)) != 0;

        HW_BMI1   = (info[1] & ((int)1 <<  3)) != 0;
        HW_BMI2   = (info[1] & ((int)1 <<  8)) != 0;
        HW_ADX    = (info[1] & ((int)1 << 19)) != 0;
        HW_SHA    = (info[1] & ((int)1 << 29)) != 0;

        HW_AVX512F  = (info[1] & ((int)1 << 16)) != 0;
        HW_AVX512CD = (info[1] & ((int)1 << 28)) != 0;
        HW_AVX512PF = (info[1] & ((int)1 << 26)) != 0;
        HW_AVX512ER = (info[1] & ((int)1 << 27)) != 0;
        HW_AVX512VL = (info[1] & ((int)1 << 31)) != 0;
        HW_AVX512BW = (info[1] & ((int)1 << 30)) != 0;
        HW_AVX512DQ = (info[1] & ((int)1 << 17)) != 0;
      }
      if (nExIds >= 0x80000001){
        cpuid(info,0x80000001);
        HW_x64   = (info[3] & ((int)1 << 29)) != 0;
        HW_ABM   = (info[2] & ((int)1 <<  5)) != 0;
        HW_SSE4a = (info[2] & ((int)1 <<  6)) != 0;
        HW_FMA4  = (info[2] & ((int)1 << 16)) != 0;
        HW_XOP   = (info[2] & ((int)1 << 11)) != 0;
      }

//
//      if (HW_XSAVE_XRSTORE && HW_AVX)
//      {
//          unsigned long long xcrFeatureMask = _xgetbv(_XCR_XFEATURE_ENABLED_MASK);
//          OS_AVX = (xcrFeatureMask & 0x6) == 0x6;
//      }


    }




  } // namespace hw
} // namespace utils




#endif /* CPUCHECK_H_ */
