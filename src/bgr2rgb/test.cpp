/**
 * @file    test.cpp
 * @ingroup
 * @author  tpan
 * @brief
 * @details
 *
 * Copyright (c) 2014 Georgia Institute of Technology.  All Rights Reserved.
 *
 * TODO add License
 */



#include "cpucheck.h"
#include <cstdio>    // printf, stderr
#include <cstring>   // memcpy
#include <stdint.h>  // uintxx_t
#include <cassert>   //assert

//#include <tmmintrin.h>  // need this?
#include <immintrin.h>  // includes all intrinsics.  compile with -march=native

#include <sys/time.h>


using namespace utils::hw;


/**
 * convert an image from bgra to rgb, assuming that the 2 images do not overlap in memory.
 */
void bgra_to_rgb_default(uint8_t* bgra, uint8_t* rgb ,
                         const uint8_t in_channels, const uint8_t out_channels,
                         const size_t w, const size_t h) {

	// restrict keyword tells compiler that bgra and rgb pointers are not aliases of each other.

  // only support in channels = 4, and out channels = 3
  assert(in_channels == 4);
  assert(out_channels == 3);

  // or not overlap at all.
  size_t nPixels = w * h;

  assert( (bgra + nPixels * in_channels * sizeof(uint8_t)) <= rgb || (rgb + nPixels * out_channels * sizeof(uint8_t)) <= bgra);

  // remaining
  for (int i = 0; i < nPixels; i++) {
    // memory copy
	  bgra += out_channels;
    *rgb++ = *--bgra;
    *rgb++ = *--bgra;
    *rgb++ = *--bgra;
    // imageData already at next pixel

    // buffer jump to next pixel
    bgra += in_channels;
  }
}

/**
 * convert an image from bgra to rgb, assuming that the 2 images do not overlap in memory.
 */
void bgra_to_rgb_bswap(uint8_t* bgra, uint8_t* rgb ,
                         const uint8_t in_channels, const uint8_t out_channels,
                         const size_t w, const size_t h) {

  // restrict keyword tells compiler that bgra and rgb pointers are not aliases of each other.

  // only support in channels = 4, and out channels = 3
  assert(in_channels == 4);
  assert(out_channels == 3);

  // or not overlap at all.
  size_t nPixels = w * h;

  assert( (bgra + nPixels * in_channels * sizeof(uint8_t)) <= rgb || (rgb + nPixels * out_channels * sizeof(uint8_t)) <= bgra);

  uint32_t* in = reinterpret_cast<uint32_t*>(bgra);
  uint32_t* end = reinterpret_cast<uint32_t*>(bgra) + nPixels;

  uint32_t x;

  // remaining
  for (; in < end; ++in) {
    // memory copy
    x = *in;

    // this code below should compile to bswap assembly instructions
    // get argb.  keep this form to get the compile to compile to bswap.
    x = (x & 0x000000ff)<<24 | (x & 0x0000ff00)<<8 | (x & 0x00ff0000)>>8 | (x & 0xff000000)>>24;
    // then shift to get rgb, and drop it in
    *(reinterpret_cast<uint32_t*>(rgb)) = x >> 8;

    rgb += out_channels;
  }
}


inline uint32_t bgra_to_rgb_kernel(const uint32_t &t) {
	uint32_t x = t;
	x = (x & 0x000000ff)<<24 | (x & 0x0000ff00)<<8 | (x & 0x00ff0000)>>8 | (x & 0xff000000)>>24;
	return (x >> 8);
}

/**
 * convert an image from bgra to rgb, assuming that the 2 images do not overlap in memory.
 */
void bgra_to_rgb_bswap_kernel(uint8_t* bgra, uint8_t* rgb ,
                         const uint8_t in_channels, const uint8_t out_channels,
                         const size_t w, const size_t h) {

  // restrict keyword tells compiler that bgra and rgb pointers are not aliases of each other.

  // only support in channels = 4, and out channels = 3
  assert(in_channels == 4);
  assert(out_channels == 3);

  // or not overlap at all.
  size_t nPixels = w * h;

  assert( (bgra + nPixels * in_channels * sizeof(uint8_t)) <= rgb || (rgb + nPixels * out_channels * sizeof(uint8_t)) <= bgra);

  uint32_t* in = reinterpret_cast<uint32_t*>(bgra);
  uint32_t* end = reinterpret_cast<uint32_t*>(bgra) + nPixels;

  uint32_t x;

  // remaining
  for (; in < end; ++in) {
    *(reinterpret_cast<uint32_t*>(rgb)) = bgra_to_rgb_kernel(*in);

    rgb += out_channels;
  }
}



void bgra_to_rgb_default_memcpy(uint8_t* bgra, uint8_t* rgb ,
                         const uint8_t in_channels, const uint8_t out_channels,
                         const size_t w, const size_t h) {

	// restrict keyword tells compiler that bgra and rgb pointers are not aliases of each other.

  // only support in channels = 4, and out channels = 3
  assert(in_channels == 4);
  assert(out_channels == 3);

  // or not overlap at all.
  size_t nPixels = w * h;

  assert( (bgra + nPixels * in_channels * sizeof(uint8_t)) <= rgb || (rgb + nPixels * out_channels * sizeof(uint8_t)) <= bgra);

  // remaining
  for (int i = 0; i < nPixels; i++) {
    // memory copy  TODO: is this actually faster?
    memcpy(rgb + 0, bgra + 2, 1);
    memcpy(rgb + 1, bgra + 1, 1);
    memcpy(rgb + 2, bgra + 0, 1);
    // imageData jump to next pixel
    rgb += out_channels;
    // buffer jump to next pixel
    bgra += in_channels;
  }
}


void bgra_to_rgb_default_memcpy2(uint8_t* bgra, uint8_t* rgb ,
                         const uint8_t in_channels, const uint8_t out_channels,
                         const size_t w, const size_t h) {

	// restrict keyword tells compiler that bgra and rgb pointers are not aliases of each other.

  // only support in channels = 4, and out channels = 3
  assert(in_channels == 4);
  assert(out_channels == 3);

  // or not overlap at all.
  size_t nPixels = w * h;

  assert( (bgra + nPixels * in_channels * sizeof(uint8_t)) <= rgb || (rgb + nPixels * out_channels * sizeof(uint8_t)) <= bgra);

  // remaining
  for (int i = 0; i < nPixels; i++) {
	  memcpy(rgb, bgra, 2);  // copies the bg to rg (so g is correct.
    memcpy(rgb, bgra + 2, 1);   // now copy r.
    memcpy(rgb + 2, bgra, 1);	// then copy b
    // imageData jump to next pixel
    rgb += out_channels;
    // buffer jump to next pixel
    bgra += in_channels;
  }
}

void bgra_to_rgb_default(uint8_t* bgra,
                         const uint8_t in_channels, const uint8_t out_channels,
                         const size_t w, const size_t h) {
	  // only support in channels = 4, and out channels = 3
	  assert(in_channels == 4);
	  assert(out_channels == 3);

	  uint8_t *rgb = bgra;

	  // either exactly overlap, or not overlap at all.
	  size_t nPixels = w * h;

	// if inplace, we only need to worry about the first 3 pixels overlapping.

	// first 3 pixels overlap
	unsigned char pixel[in_channels];
	for (int i = 0; i < 3; ++i) {
	  memcpy(pixel, bgra, in_channels);   // copy to temp location
	  // note that element 1 is not move at all.
	  pixel[3] = pixel[0];  // save b in a's position
	  pixel[0] = pixel[2];  // move r into b's original position
	  pixel[2] = pixel[3];  // move saved b into r's original position
	  memcpy(rgb, pixel, out_channels);
	  // imageData jump to next pixel
	  rgb += out_channels;
	  // buffer jump to next pixel
	  bgra += in_channels;
	}


  // remaining
  for (int i = 3; i < nPixels; i++) {
//	// memory copy  TODO: is this actually faster?
//	memcpy(rgb + 0, bgra + 2, 1);
//	memcpy(rgb + 1, bgra + 1, 1);
//	memcpy(rgb + 2, bgra + 0, 1);
//	// imageData jump to next pixel
//	rgb += out_channels;
	    // memory copy
		  bgra += out_channels;
	    *rgb++ = *--bgra;
	    *rgb++ = *--bgra;
	    *rgb++ = *--bgra;
	    // imageData already at next pixel

	// buffer jump to next pixel
	bgra += in_channels;
  }

}

#ifdef __SSSE3__

void bgra_to_rgb_ssse3(uint8_t* bgra, uint8_t* rgb,
        const uint8_t in_channels, const uint8_t out_channels,
        const size_t w, const size_t h) {

  // only support in channels = 4, and out channels = 3
  assert(in_channels == 4);
  assert(out_channels == 3);

  // either exactly overlap, or not overlap at all.
  size_t nPixels = w * h;

  assert((bgra + nPixels * in_channels * sizeof(uint8_t)) <= rgb || (rgb + nPixels * out_channels * sizeof(uint8_t)) <= bgra);

  // mask initialized from byte 15 to byte 0.  0x80 sets value to 0.
  __m128i mask = _mm_set_epi8(15 | 0x80, 11 | 0x80, 7 | 0x80, 3 | 0x80, 12, 13, 14, 8, 9, 10, 4, 5, 6, 0, 1, 2);

  // because we are going in from 16 to 12 bytes, memory access for the output array is not going to be aligned
  bool input_aligned = (size_t)bgra % 16 == 0;

  // change input and output to xmm128*
  __m128i *in = (__m128i*)bgra;


  // call shuffle function.
  size_t nloads = nPixels / (16 / in_channels);
  size_t remainder = nPixels % (16 / in_channels);
  // first nloads * 4 pixels
  if (input_aligned) {
	  for (int i = 0; i < nloads; i++) {
		 _mm_storeu_si128((__m128i*)rgb, _mm_shuffle_epi8(in[i], mask));
		 rgb += 12;
	  }
	  bgra += nloads * 16;
  } else {
	  for (int i = 0; i < nloads; i++) {
		 _mm_storeu_si128((__m128i*)rgb,
				 _mm_shuffle_epi8(_mm_loadu_si128((__m128i*)bgra), mask));
		 bgra += 16;
		 rgb += 12;
	  }
  }
  // remaining pixels
  uint32_t* in4 = reinterpret_cast<uint32_t*>(bgra);
  uint32_t* end = reinterpret_cast<uint32_t*>(bgra) + remainder;

  uint32_t x;

  // remaining
  for (; in4 < end; ++in4) {
    // memory copy
    x = *in4;

    // this code below should compile to bswap assembly instructions
    // get argb.  keep this form to get the compile to compile to bswap.
    x = (x & 0x000000ff)<<24 | (x & 0x0000ff00)<<8 | (x & 0x00ff0000)>>8 | (x & 0xff000000)>>24;
    // then shift to get rgb, and drop it in
    *(reinterpret_cast<uint32_t*>(rgb)) = x >> 8;

    rgb += out_channels;
  }


}


void bgra_to_rgb_ssse3(uint8_t* bgra,
        const uint8_t in_channels, const uint8_t out_channels,
        const size_t w, const size_t h) {

  // only support in channels = 4, and out channels = 3
  assert(in_channels == 4);
  assert(out_channels == 3);

  // either exactly overlap, or not overlap at all.
  size_t nPixels = w * h;

  // mask initialized from byte 15 to byte 0.  0x80 sets value to 0.
  __m128i mask = _mm_set_epi8(15 | 0x80, 11 | 0x80, 7 | 0x80, 3 | 0x80, 12, 13, 14, 8, 9, 10, 4, 5, 6, 0, 1, 2);

  // because we are going in from 16 to 12 bytes, memory access for the output array is not going to be aligned
  bool input_aligned = (size_t)bgra % 16 == 0;

  // change input and output to xmm128*
  __m128i *in = (__m128i*)bgra;
  uint8_t* rgb = bgra;

  // call shuffle function.
  size_t nloads = nPixels / (16 / in_channels);
  size_t remainder = nPixels % (16 / in_channels);
  // first nloads * 4 pixels
  if (input_aligned) {
	  for (int i = 0; i < nloads; i++) {
		 _mm_storeu_si128((__m128i*)rgb, _mm_shuffle_epi8(in[i], mask));
		 rgb += 12;
	  }
	  bgra += nloads * 16;
  } else {
	  for (int i = 0; i < nloads; i++) {
		 _mm_storeu_si128((__m128i*)rgb,
				 _mm_shuffle_epi8(_mm_loadu_si128((__m128i*)bgra), mask));
		 bgra += 16;
		 rgb += 12;
	  }
  }
  // remaining pixels
  uint32_t* in4 = reinterpret_cast<uint32_t*>(bgra);
  uint32_t* end = reinterpret_cast<uint32_t*>(bgra) + remainder;

  uint32_t x;

  // remaining
  for (; in4 < end; ++in4) {
    // memory copy
    x = *in4;

    // this code below should compile to bswap assembly instructions
    // get argb.  keep this form to get the compile to compile to bswap.
    x = (x & 0x000000ff)<<24 | (x & 0x0000ff00)<<8 | (x & 0x00ff0000)>>8 | (x & 0xff000000)>>24;
    // then shift to get rgb, and drop it in
    *(reinterpret_cast<uint32_t*>(rgb)) = x >> 8;

    rgb += out_channels;
  }

}

#endif

#ifdef __AVX2__

//#include <immintrin.h>
void bgra_to_rgb_avx2(uint8_t* bgra, uint8_t* rgb,
        const uint8_t in_channels, const uint8_t out_channels,
        const size_t w, const size_t h) {

  // only support in channels = 4, and out channels = 3
  assert(in_channels == 4);
  assert(out_channels == 3);

  // either exactly overlap, or not overlap at all.
  size_t nPixels = w * h;

  assert((bgra + nPixels * in_channels * sizeof(uint8_t)) <= rgb || (rgb + nPixels * out_channels * sizeof(uint8_t)) <= bgra);

  // mask initialized from byte 31 to byte 0.  0x80 sets value to 0.
  __m256i mask = _mm256_set_epi8(31 | 0x80, 27 | 0x80, 23 | 0x80, 19 | 0x80, 15 | 0x80, 11 | 0x80, 7 | 0x80, 3 | 0x80, 28, 29, 30, 24, 25, 26, 20, 21, 22, 16, 17, 18, 12, 13, 14, 8, 9, 10, 4, 5, 6, 0, 1, 2);

  // because we are going in from 32 to 24 bytes, memory access for the output array is not going to be aligned
  bool input_aligned = (size_t)bgra % 32 == 0;

  // change input and output to xmm128*
  __m256i *in = (__m256i*)bgra;


  // call shuffle function.
  size_t nloads = nPixels / (32 / in_channels);
  size_t remainder = nPixels % (32 / in_channels);
  // first nloads * 8 pixels
  if (input_aligned) {
	  for (int i = 0; i < nloads; i++) {
		 _mm256_storeu_si256((__m256i*)rgb, _mm256_shuffle_epi8(in[i], mask));
		 rgb += 24;
	  }
	  bgra += nloads * 32;
  } else {
	  for (int i = 0; i < nloads; i++) {
		 _mm256_storeu_si256((__m256i*)rgb,
				 _mm256_shuffle_epi8(_mm256_loadu_si256((__m256i*)bgra), mask));
		 bgra += 32;
		 rgb += 24;
	  }
  }
  // remaining pixels
  uint32_t* in4 = reinterpret_cast<uint32_t*>(bgra);
  uint32_t* end = reinterpret_cast<uint32_t*>(bgra) + remainder;

  uint32_t x;

  // remaining
  for (; in4 < end; ++in4) {
    // memory copy
    x = *in4;

    // this code below should compile to bswap assembly instructions
    // get argb.  keep this form to get the compile to compile to bswap.
    x = (x & 0x000000ff)<<24 | (x & 0x0000ff00)<<8 | (x & 0x00ff0000)>>8 | (x & 0xff000000)>>24;
    // then shift to get rgb, and drop it in
    *(reinterpret_cast<uint32_t*>(rgb)) = x >> 8;

    rgb += out_channels;
  }

}


void bgra_to_rgb_avx2(uint8_t* bgra,
        const uint8_t in_channels, const uint8_t out_channels,
        const size_t w, const size_t h) {

  // only support in channels = 4, and out channels = 3
  assert(in_channels == 4);
  assert(out_channels == 3);

  // either exactly overlap, or not overlap at all.
  size_t nPixels = w * h;

  // mask initialized from byte 31 to byte 0.  0x80 sets value to 0.
  __m256i mask = _mm256_set_epi8(31 | 0x80, 27 | 0x80, 23 | 0x80, 19 | 0x80, 15 | 0x80, 11 | 0x80, 7 | 0x80, 3 | 0x80, 28, 29, 30, 24, 25, 26, 20, 21, 22, 16, 17, 18, 12, 13, 14, 8, 9, 10, 4, 5, 6, 0, 1, 2);

  // because we are going in from 16 to 12 bytes, memory access for the output array is not going to be aligned
  bool input_aligned = (size_t)bgra % 32 == 0;

  // change input and output to xmm128*
  __m256i *in = (__m256i*)bgra;
  uint8_t* rgb = bgra;

  // call shuffle function.
  size_t nloads = nPixels / (32 / in_channels);
  size_t remainder = nPixels % (32 / in_channels);
  // first nloads * 8 pixels
  if (input_aligned) {
	  for (int i = 0; i < nloads; i++) {
		 _mm256_storeu_si256((__m256i*)rgb, _mm256_shuffle_epi8(in[i], mask));
		 rgb += 24;
	  }
	  bgra += nloads * 32;
  } else {
	  for (int i = 0; i < nloads; i++) {
		 _mm256_storeu_si256((__m256i*)rgb,
				 _mm256_shuffle_epi8(_mm256_loadu_si256((__m256i*)bgra), mask));
		 bgra += 32;
		 rgb += 24;
	  }
  }
  // remaining pixels
  uint32_t* in4 = reinterpret_cast<uint32_t*>(bgra);
  uint32_t* end = reinterpret_cast<uint32_t*>(bgra) + remainder;

  uint32_t x;

  // remaining
  for (; in4 < end; ++in4) {
    // memory copy
    x = *in4;

    // this code below should compile to bswap assembly instructions
    // get argb.  keep this form to get the compile to compile to bswap.
    x = (x & 0x000000ff)<<24 | (x & 0x0000ff00)<<8 | (x & 0x00ff0000)>>8 | (x & 0xff000000)>>24;
    // then shift to get rgb, and drop it in
    *(reinterpret_cast<uint32_t*>(rgb)) = x >> 8;

    rgb += out_channels;
  }

}

#endif

void bgra_to_rgb_avx512(uint8_t* bgra, uint8_t* rgb,
        const uint8_t in_channels, const uint8_t out_channels,
        const size_t w, const size_t h) {
  // not tested or implemented.
  bool not_implemented = false;
  assert(not_implemented);
}

int main(int argc, char** argv) {

  checkCPUCapability();


//  fprintf(stderr, "mmx: %s\n",    (HW_MMX   ? "true" : "false"));
//  fprintf(stderr, "sse: %s\n",    (HW_SSE   ? "true" : "false"));
//  fprintf(stderr, "sse2: %s\n",   (HW_SSE2  ? "true" : "false"));
//  fprintf(stderr, "sse3: %s\n",   (HW_SSE3  ? "true" : "false"));
  fprintf(stderr, "Support SSSE3: %s\n",  (HW_SSSE3 ? "true" : "false"));
//  fprintf(stderr, "sse41: %s\n",  (HW_SSE41 ? "true" : "false"));
//  fprintf(stderr, "sse42: %s\n",  (HW_SSE42 ? "true" : "false"));
//  fprintf(stderr, "sse4a: %s\n",  (HW_SSE4a ? "true" : "false"));
//  fprintf(stderr, "avx: %s\n",    (HW_AVX   ? "true" : "false"));
//  fprintf(stderr, "os avx: %s\n", (OS_AVX   ? "true" : "false"));
  fprintf(stderr, "Support AVX2: %s\n",   (HW_AVX2  ? "true" : "false"));
//  fprintf(stderr, "fma3: %s\n",   (HW_FMA3  ? "true" : "false"));
//  fprintf(stderr, "fma4: %s\n",   (HW_FMA4  ? "true" : "false"));
  fprintf(stderr, "Support AVX512BW: %s\n", (HW_AVX512BW ? "true" : "false"));

// GCC only defines __MMX__, __SSE__, and __SSE2__


  struct timeval  tv1, tv2;

  // create some synthetic data, for a reasonable size image
  size_t w = 4096;
  size_t h = 4096;
  int bgra_channels = 4;
  int rgb_channels = 3;

  int alignment = 32;   // 64 for Knights Landing.  32 for AVX2, 16 for SSSE3
  size_t bgra_size = w * h * bgra_channels * sizeof(uint8_t);
  bgra_size = ((bgra_size + alignment - 1) / alignment) * alignment;  // get integral multiple of alignment
  size_t rgb_size = w * h * rgb_channels * sizeof(uint8_t);
  rgb_size = ((rgb_size + alignment - 1) / alignment) * alignment;  // get integral multiple of alignment

  // also see aligned_alloc, aligned_storage, __attribute__((aligned)) or __attribute__((aligned(32))), and alignas(32) char[32]
  uint8_t *bgra_image = (uint8_t *)aligned_alloc(alignment, bgra_size);
  uint8_t *rgb_image = (uint8_t *)aligned_alloc(alignment, rgb_size);
  memset(bgra_image, 0, bgra_size);
  memset(rgb_image, 0, rgb_size);


  // ============ not in place, default imple.
  for (int i = 0; i < bgra_size ; ++i) {
	  bgra_image[i] = i%4 + 1;
  }

  gettimeofday(&tv1, NULL);

  // call the function to convert
  bgra_to_rgb_default(bgra_image, rgb_image,
                      bgra_channels, rgb_channels,
                      w, h);
  gettimeofday(&tv2, NULL);

  double duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
           (double) (tv2.tv_sec - tv1.tv_sec);

  // verify the results
  bool correct =  true;
  for (int i = 0; i < rgb_size; ++i) {
	  correct &= rgb_image[i] == (3 - i%3);
  }
  if (!correct) printf("*FAIL:  TEST bgra to rgb using default SISD algo, NOT in place. time %f sec\n", duration);
  else printf("PASS:  TEST bgra to rgb using default SISD algo, NOT in place. time %f sec\n", duration);

  // ============ in place, default imple.
  memset(bgra_image, 0, bgra_size);
  for (int i = 0; i < bgra_size ; ++i) {
	  bgra_image[i] = i%4 + 1;
  }

  gettimeofday(&tv1, NULL);

  // call the function to convert
  bgra_to_rgb_default(bgra_image,
                           bgra_channels, rgb_channels,
                           w, h);
  gettimeofday(&tv2, NULL);

  duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
           (double) (tv2.tv_sec - tv1.tv_sec);

  // verify the results
  correct =  true;
  for (int i = 0; i < rgb_size; ++i) {
	  correct &= bgra_image[i] == (3 - i%3);
  }
  if (!correct) printf("*FAIL:  TEST bgra to rgb using default SISD algo, in place. time %f sec\n", duration);
  else printf("PASS:  TEST bgra to rgb using default SISD algo, in place. time %f sec\n", duration);


  // ============ in place, memcpy imple.
  memset(bgra_image, 0, bgra_size);
  for (int i = 0; i < bgra_size ; ++i) {
	  bgra_image[i] = i%4 + 1;
  }
  memset(rgb_image, 0, rgb_size);

  gettimeofday(&tv1, NULL);

  // call the function to convert
  bgra_to_rgb_default_memcpy(bgra_image, rgb_image,
                           bgra_channels, rgb_channels,
                           w, h);
  gettimeofday(&tv2, NULL);

  duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
           (double) (tv2.tv_sec - tv1.tv_sec);

  // verify the results
  correct =  true;
  for (int i = 0; i < rgb_size; ++i) {
	  correct &= rgb_image[i] == (3 - i%3);
  }
  if (!correct) printf("*FAIL: TEST bgra to rgb using SISD memcpy, NOT in place. time %f sec\n", duration);
  else printf("PASS:  TEST bgra to rgb using SISD memcpy, NOT in place. time %f sec\n", duration);




  // ============ in place, memcpy imple.
  for (int i = 0; i < bgra_size ; ++i) {
    bgra_image[i] = i%4 + 1;
  }

  gettimeofday(&tv1, NULL);

  // call the function to convert
  bgra_to_rgb_default_memcpy2(bgra_image, rgb_image,
                           bgra_channels, rgb_channels,
                           w, h);
  gettimeofday(&tv2, NULL);

  duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
           (double) (tv2.tv_sec - tv1.tv_sec);

  // verify the results
  correct =  true;
  for (int i = 0; i < rgb_size; ++i) {
	  correct &= rgb_image[i] == (3 - i%3);
  }
  if (!correct) printf("*FAIL: TEST bgra to rgb using fast SISD memcpy, NOT in place. time %f sec\n", duration);
  else printf("PASS:  TEST bgra to rgb using fast SISD memcpy, NOT in place. time %f sec\n", duration);

  // ============ in place, memcpy imple.
  for (int i = 0; i < bgra_size ; ++i) {
    bgra_image[i] = i%4 + 1;
  }

  gettimeofday(&tv1, NULL);

  // call the function to convert
  bgra_to_rgb_bswap(bgra_image, rgb_image,
                           bgra_channels, rgb_channels,
                           w, h);
  gettimeofday(&tv2, NULL);

  duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
           (double) (tv2.tv_sec - tv1.tv_sec);

  // verify the results
  correct =  true;
  for (int i = 0; i < rgb_size; ++i) {
    correct &= rgb_image[i] == (3 - i%3);
  }
  if (!correct) printf("*FAIL: TEST bgra to rgb using SISD bswap, NOT in place. time %f sec\n", duration);
  else printf("PASS:  TEST bgra to rgb using SISD bswap, NOT in place. time %f sec\n", duration);


  // ============ in place, memcpy imple.
  for (int i = 0; i < bgra_size ; ++i) {
    bgra_image[i] = i%4 + 1;
  }

  gettimeofday(&tv1, NULL);

  // call the function to convert
  bgra_to_rgb_bswap_kernel(bgra_image, rgb_image,
                           bgra_channels, rgb_channels,
                           w, h);
  gettimeofday(&tv2, NULL);

  duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
           (double) (tv2.tv_sec - tv1.tv_sec);

  // verify the results
  correct =  true;
  for (int i = 0; i < rgb_size; ++i) {
    correct &= rgb_image[i] == (3 - i%3);
  }
  if (!correct) printf("*FAIL: TEST bgra to rgb using SISD bswap kernel, NOT in place. time %f sec\n", duration);
  else printf("PASS:  TEST bgra to rgb using SISD bswap kernel, NOT in place. time %f sec\n", duration);


#ifdef __SSSE3__

  //====== ssse3
  // ============ not in place, default imple.
  for (int i = 0; i < bgra_size ; ++i) {
	  bgra_image[i] = i%4 + 1;
  }

  gettimeofday(&tv1, NULL);

  // call the function to convert
  bgra_to_rgb_ssse3(bgra_image, rgb_image,
                      bgra_channels, rgb_channels,
                      w, h);
  gettimeofday(&tv2, NULL);

  duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
           (double) (tv2.tv_sec - tv1.tv_sec);

  // verify the results
  correct =  true;
  for (int i = 0; i < rgb_size; ++i) {
	  correct &= rgb_image[i] == (3 - i%3);
  }
  if (!correct) printf("*FAIL:  TEST bgra to rgb using SSSE3 algo, NOT in place. time %f sec\n", duration);
  else printf("PASS:  TEST bgra to rgb using SSSE3 algo, NOT in place. time %f sec\n", duration);

  // ============ in place, default imple.
  memset(bgra_image, 0, bgra_size);
  for (int i = 0; i < bgra_size ; ++i) {
	  bgra_image[i] = i%4 + 1;
  }

  gettimeofday(&tv1, NULL);

  // call the function to convert
  bgra_to_rgb_ssse3(bgra_image,
                           bgra_channels, rgb_channels,
                           w, h);
  gettimeofday(&tv2, NULL);

  duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
           (double) (tv2.tv_sec - tv1.tv_sec);

  // verify the results
  correct =  true;
  for (int i = 0; i < rgb_size; ++i) {
	  correct &= bgra_image[i] == (3 - i%3);
  }
  if (!correct) printf("*FAIL:  TEST bgra to rgb using SSSE3 algo, in place. time %f sec\n", duration);
  else printf("PASS:  TEST bgra to rgb using SSSE3 algo, in place. time %f sec\n", duration);

#endif

#ifdef __AVX2__
  //====== avx2
  // ============ not in place, default imple.
  for (int i = 0; i < bgra_size ; ++i) {
	  bgra_image[i] = i%4 + 1;
  }

  gettimeofday(&tv1, NULL);

  // call the function to convert
  bgra_to_rgb_avx2(bgra_image, rgb_image,
                      bgra_channels, rgb_channels,
                      w, h);
  gettimeofday(&tv2, NULL);

  duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
           (double) (tv2.tv_sec - tv1.tv_sec);

  // verify the results
  correct =  true;
  for (int i = 0; i < rgb_size; ++i) {
	  correct &= rgb_image[i] == (3 - i%3);
  }
  if (!correct) printf("*FAIL:  TEST bgra to rgb using AVX2 algo, NOT in place. time %f sec\n", duration);
  else printf("PASS:  TEST bgra to rgb using AVX2 algo, NOT in place. time %f sec\n", duration);

  // ============ in place, default imple.
  memset(bgra_image, 0, bgra_size);
  for (int i = 0; i < bgra_size ; ++i) {
	  bgra_image[i] = i%4 + 1;
  }

  gettimeofday(&tv1, NULL);

  // call the function to convert
  bgra_to_rgb_avx2(bgra_image,
                           bgra_channels, rgb_channels,
                           w, h);
  gettimeofday(&tv2, NULL);

  duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
           (double) (tv2.tv_sec - tv1.tv_sec);

  // verify the results
  correct =  true;
  for (int i = 0; i < rgb_size; ++i) {
	  correct &= bgra_image[i] == (3 - i%3);
  }
  if (!correct) printf("*FAIL:  TEST bgra to rgb using AVX2 algo, in place. time %f sec\n", duration);
  else printf("PASS:  TEST bgra to rgb using AVX2 algo, in place. time %f sec\n", duration);


#endif

  free(bgra_image);
  free(rgb_image);

  return 0;
}
