/**
 * @file    test.cpp
 * @ingroup
 * @author  tpan
 * @brief
 * @details
 *
 * Copyright (c) 2014 Georgia Institute of Technology.  All Rights Reserved.
 *
 * TODO add License
 */


#include <cstdio>
#include <cstdlib>    // exit
#include <cmath>
#include <cstring>
#include <stdint.h>  // uintxx_t
#include <cassert>   //assert
#include <fcntl.h>   // open
#include <sys/stat.h>
#include <sys/types.h> // err_sys
#include <sys/mman.h> // mmap()
#include <unistd.h>  //lseek, write

#include <sys/time.h>

// test different downsample codes


inline uint32_t downsample_average_kernel(const uint32_t& a, const uint32_t& b, const uint32_t& c, const uint32_t& d) {
	uint32_t t1, t2;
	t1 = (((a ^ b) & 0xFEFEFEFEL) >> 1) + (a & b);
	t2 = (((c ^ d) & 0xFEFEFEFEL) >> 1) + (c & d);
	return (((t1 ^ t2) & 0xFEFEFEFEL) >> 1) + (t1 & t2);
}

//
//inline uint32_t downsample_average_kernel(const uint32_t& a, const uint32_t& b, const uint32_t& c, const uint32_t& d) {
//	uint32_t x, y, z ,w;
//	x = a ^ b; y = c ^ d;
//	z = a & b; w = c & d;
//	return = (((x ^ y) & 0xFCFCFCFC) >> 2) + ((x & y & 0xFEFEFEFE) >> 1) + (((z ^ w) & 0xFEFEFEFE) >> 1) + (z & w);
//}


inline uint32_t downsample_average_kernel_4(const uint64_t& a, const uint64_t& c) {
	uint64_t t1 = (((a ^ c) & 0xFEFEFEFEFEFEFEFE) >> 1) + (a & c);
	uint32_t t2 = t1 & 0x00000000FFFFFFFF;
	uint32_t t3 = t1 >> 32;
	return (((t3 ^ t2) & 0xFEFEFEFEL) >> 1) + (t3 & t2);
}

inline uint32_t downsample_average_kernel_3(const uint64_t& a, const uint64_t& c) {
	uint64_t t1 = (((a ^ c) & 0xFEFEFEFEFEFEFEFE) >> 1) + (a & c);
	uint32_t t2 = t1 & 0x0000000000FFFFFF;
	uint32_t t3 = (t1 >> 24) & 0x0000000000FFFFFF;
	return (((t3 ^ t2) & 0xFEFEFEFEL) >> 1) + (t3 & t2);
}


inline uint32_t downsample_average_kernel_1D(const uint32_t& a, const uint32_t& b) {
	return (((a ^ b) & 0xFEFEFEFEL) >> 1) + (a & b);
}



void downsample_average (uint32_t* output,
                           uint32_t* input, const size_t lw, const size_t lh,
                           const uint32_t downSamplingFactor) {

	assert(downSamplingFactor >= 2);

	// do one 1/2 sample run
	// (a + b) / 2 = ((a ^ b) >> 1) + (a & b)
	uint32_t t1, t2, a, b;
	uint32_t *row1 = input, *row2 = input + lw,
			*dest = downSamplingFactor == 2 ? output : input;  // if last recursion, put in output, else do it in place
	for (int j = 0; j < lh; j+= 2) {

		for (int i = 0; i < lw; i +=2) {
			*dest++ = downsample_average_kernel(*row1++, *row1++, *row2++, *row2++);
		}
		row1 = row2;  // end of row2 is next row 1
		row2 += lw;
	}
	// at this point, input has been averaged and stored in first 1/4 of bytes.
	// since we stride forward 2 col and rows at a time, we don't need to worry about overwriting an unread pixel.

	// recurse
	if (downSamplingFactor > 2) {
		downsample_average(output, input, lw >> 1, lh >> 1, downSamplingFactor >> 1);
	} // else exactly 2, then done.
}




void downsample_average_bytes (uint8_t* output, const unsigned int channels,
                           uint8_t* input, const size_t lw, const size_t lh, const unsigned int lchannels,
                           const uint32_t downSamplingFactor) {

	assert(downSamplingFactor >= 2);

	// do one 1/2 sample run
	// (a + b) / 2 = ((a ^ b) >> 1) + (a & b)
	uint32_t t1, t2, a, b;
	uint8_t *row1 = input, *row2 = input + lw * lchannels,
			*dest = (downSamplingFactor == 2 ? output : input);  // if last recursion, put in output, else do it in place
	uint32_t c = (downSamplingFactor == 2 ? channels : lchannels);  // if last recursion, do pixel changes.

	int i = 0, j = 0;
	// skip last row, as the very last dest element may have overflow.
	for (j = 0; j < lh - 2; j += 2) {

		for (i = 0; i < lw; i +=2) {
			// if dest == input, this write is fine.  else dest may have 1 byte written out of bound.
			*(reinterpret_cast<uint32_t*>(dest)) = downsample_average_kernel(
					*(reinterpret_cast<uint32_t*>(row1)),
					*(reinterpret_cast<uint32_t*>(row1 + lchannels)),
					*(reinterpret_cast<uint32_t*>(row2)),
					*(reinterpret_cast<uint32_t*>(row2 + lchannels)));

			dest += c;
			row1 += 2 * lchannels;
			row2 += 2 * lchannels;
		}
		row1 = row2;  // end of row2 is next row 1
		row2 += lw * lchannels;
	}
	// at this point, input has been averaged and stored in first 1/4 of bytes.
	// since we stride forward 2 col and rows at a time, we don't need to worry about overwriting an unread pixel.

	// do the last row.  skip the last lement.
	for (i = 0; i < lw - 2; i +=2) {
		*(reinterpret_cast<uint32_t*>(dest)) = downsample_average_kernel(
				*(reinterpret_cast<uint32_t*>(row1)),
				*(reinterpret_cast<uint32_t*>(row1 + lchannels)),
				*(reinterpret_cast<uint32_t*>(row2)),
				*(reinterpret_cast<uint32_t*>(row2 + lchannels)));

		dest += c;
		row1 += 2 * lchannels;
		row2 += 2 * lchannels;
	}

	// last element
	a = downsample_average_kernel(
					*(reinterpret_cast<uint32_t*>(row1)),
					*(reinterpret_cast<uint32_t*>(row1 + lchannels)),
					*(reinterpret_cast<uint32_t*>(row2)),
					*(reinterpret_cast<uint32_t*>(row2 + lchannels)));
	// if dest == input, this write is fine.  else dest may have 1 byte written out of bound.
	memcpy(dest, reinterpret_cast<uint8_t*>(&a), c);


	// recurse
	if (downSamplingFactor > 2) {
		downsample_average_bytes(output, channels, input, lw >> 1, lh >> 1, lchannels, downSamplingFactor >> 1);
	} // else exactly 2, then done.
}



// row to row average first, then col. access.  fewer unaligned memory access.
void downsample_average_bytes_2pass (uint8_t* output, const unsigned int channels,
                           uint8_t* input, const size_t lw, const size_t lh, const unsigned int lchannels,
                           const uint32_t downSamplingFactor) {

	assert(downSamplingFactor >= 2);

	// do one 1/2 sample run
	// (a + b) / 2 = ((a ^ b) >> 1) + (a & b)
	uint32_t t1, t2, a, b;
	// size of row in number of bytes, inflated to multiple of 16 bytes
	size_t temprowsize = ((lw * lchannels + sizeof(long long) - 1) / sizeof(long long)) * sizeof (long long);
	uint8_t* trow = new uint8_t[temprowsize];

	size_t intsInRow = (lw * lchannels + sizeof(uint32_t) - 1) / sizeof(uint32_t);

	uint32_t *row1 = reinterpret_cast<uint32_t*>(input),
			 *row2 = reinterpret_cast<uint32_t*>(input + lw * lchannels);
	uint8_t	*dest = (downSamplingFactor == 2 ? output : input);  // if last recursion, put in output, else do it in place
	uint32_t c = (downSamplingFactor == 2 ? channels : lchannels);  // if last recursion, do pixel changes.

	int i = 0, j = 0;
	uint8_t* trow_pos;
	// skip last row, as the very last dest element may have overflow.
	for (j = 0; j < lh - 2; j += 2) {

		// first average 2 rows.
		trow_pos = trow;
		row1 = reinterpret_cast<uint32_t*>(input + j * lw * lchannels);
		row2 = reinterpret_cast<uint32_t*>(input + (j+1) * lw * lchannels);

		for (i = 0; i < intsInRow; ++i, trow_pos += sizeof(uint32_t)) {
			*(reinterpret_cast<uint32_t*>(trow_pos)) = downsample_average_kernel_1D(*row1++, *row2++);
		}

		// then work on combining the columns
		trow_pos = trow;
		for (i = 0; i < lw; i += 2, trow_pos += 2 * lchannels, dest += c) {
			*(reinterpret_cast<uint32_t*>(dest)) = downsample_average_kernel_1D(
					*(reinterpret_cast<uint32_t*>(trow_pos)),
					*(reinterpret_cast<uint32_t*>(trow_pos + lchannels)));
		}

	}
	// at this point, input has been averaged and stored in first 1/4 of bytes.
	// since we stride forward 2 col and rows at a time, we don't need to worry about overwriting an unread pixel.

	// do the last row.  skip the last element.  because of special cases at the last element and don't want illegal access, do it the old way.
	row1 = reinterpret_cast<uint32_t*>(input + j * lw * lchannels);
	row2 = reinterpret_cast<uint32_t*>(input + (j+1) * lw * lchannels);
	for (i = 0; i < lw - 2; i +=2) {
		*(reinterpret_cast<uint32_t*>(dest)) = downsample_average_kernel(
				*(reinterpret_cast<uint32_t*>(row1)),
				*(reinterpret_cast<uint32_t*>(row1 + lchannels)),
				*(reinterpret_cast<uint32_t*>(row2)),
				*(reinterpret_cast<uint32_t*>(row2 + lchannels)));

		dest += c;
		row1 += 2 * lchannels;
		row2 += 2 * lchannels;
	}

	// last element
	a = downsample_average_kernel(
					*(reinterpret_cast<uint32_t*>(row1)),
					*(reinterpret_cast<uint32_t*>(row1 + lchannels)),
					*(reinterpret_cast<uint32_t*>(row2)),
					*(reinterpret_cast<uint32_t*>(row2 + lchannels)));
	// if dest == input, this write is fine.  else dest may have 1 byte written out of bound.
	memcpy(dest, reinterpret_cast<uint8_t*>(&a), c);


	// recurse
	if (downSamplingFactor > 2) {
		downsample_average_bytes_2pass(output, channels, input, lw >> 1, lh >> 1, lchannels, downSamplingFactor >> 1);
	} // else exactly 2, then done.
	delete [] trow;
}

// row to row average first, then col. access.  fewer unaligned memory access.
void downsample_average_bytes_2pass_4 (uint8_t* output, const unsigned int channels,
                           uint8_t* input, const size_t lw, const size_t lh, const unsigned int lchannels,
                           const uint32_t downSamplingFactor) {

	assert(downSamplingFactor >= 2);

	// do one 1/2 sample run
	// (a + b) / 2 = ((a ^ b) >> 1) + (a & b)
	uint32_t t1, t2, a, b;

	size_t longsInRow = (lw * lchannels + sizeof(uint64_t) - 1) / sizeof(uint64_t);

	uint8_t *row1 = input,
			*row2 = input + lw * lchannels;
	uint8_t	*dest = (downSamplingFactor == 2 ? output : input);  // if last recursion, put in output, else do it in place
	uint32_t c = (downSamplingFactor == 2 ? channels : lchannels);  // if last recursion, do pixel changes.

	int i = 0, j = 0;
	// skip last row, as the very last dest element may have overflow.
	for (j = 0; j < lh - 2; j += 2) {

		for (i = 0; i < lw; i += 2) {
			*(reinterpret_cast<uint32_t*>(dest)) = downsample_average_kernel_4(
					*(reinterpret_cast<uint64_t*>(row1)),
					*(reinterpret_cast<uint64_t*>(row2)));
			dest += c;
			row1 += 2 * lchannels;
			row2 += 2 * lchannels;
		}
		row1 = row2;
		row2 += lw * lchannels;
	}
	// at this point, input has been averaged and stored in first 1/4 of bytes.
	// since we stride forward 2 col and rows at a time, we don't need to worry about overwriting an unread pixel.

	// do the last row.  skip the last element.  because of special cases at the last element and don't want illegal access, do it the old way.
	for (i = 0; i < lw - 2; i +=2) {
		*(reinterpret_cast<uint32_t*>(dest)) = downsample_average_kernel(
				*(reinterpret_cast<uint32_t*>(row1)),
				*(reinterpret_cast<uint32_t*>(row1 + lchannels)),
				*(reinterpret_cast<uint32_t*>(row2)),
				*(reinterpret_cast<uint32_t*>(row2 + lchannels)));

		dest += c;
		row1 += 2 * lchannels;
		row2 += 2 * lchannels;
	}

	// last element
	a = downsample_average_kernel(
					*(reinterpret_cast<uint32_t*>(row1)),
					*(reinterpret_cast<uint32_t*>(row1 + lchannels)),
					*(reinterpret_cast<uint32_t*>(row2)),
					*(reinterpret_cast<uint32_t*>(row2 + lchannels)));
	// if dest == input, this write is fine.  else dest may have 1 byte written out of bound.
	memcpy(dest, reinterpret_cast<uint8_t*>(&a), c);


	// recurse
	if (downSamplingFactor > 2) {
		downsample_average_bytes_2pass_4(output, channels, input, lw >> 1, lh >> 1, lchannels, downSamplingFactor >> 1);
	} // else exactly 2, then done.
}

// row to row average first, then col. access.  fewer unaligned memory access.
void downsample_average_bytes_2pass_3 (uint8_t* output, const unsigned int channels,
                           uint8_t* input, const size_t lw, const size_t lh, const unsigned int lchannels,
                           const uint32_t downSamplingFactor) {

	assert(downSamplingFactor >= 2);

	// do one 1/2 sample run
	// (a + b) / 2 = ((a ^ b) >> 1) + (a & b)
	uint32_t t1, t2, a, b;

	size_t longsInRow = (lw * lchannels + sizeof(uint64_t) - 1) / sizeof(uint64_t);

	uint8_t *row1 = input,
			*row2 = input + lw * lchannels;
	uint8_t	*dest = (downSamplingFactor == 2 ? output : input);  // if last recursion, put in output, else do it in place
	uint32_t c = (downSamplingFactor == 2 ? channels : lchannels);  // if last recursion, do pixel changes.

	int i = 0, j = 0;
	// skip last row, as the very last dest element may have overflow.
	for (j = 0; j < lh - 2; j += 2) {

		for (i = 0; i < lw; i += 2) {
			*(reinterpret_cast<uint32_t*>(dest)) = downsample_average_kernel_3(
					*(reinterpret_cast<uint64_t*>(row1)),
					*(reinterpret_cast<uint64_t*>(row2)));
			dest += c;
			row1 += 2 * lchannels;
			row2 += 2 * lchannels;
		}
		row1 = row2;
		row2 += lw * lchannels;
	}
	// at this point, input has been averaged and stored in first 1/4 of bytes.
	// since we stride forward 2 col and rows at a time, we don't need to worry about overwriting an unread pixel.

	// do the last row.  skip the last element.  because of special cases at the last element and don't want illegal access, do it the old way.

	for (i = 0; i < lw - 2; i +=2) {
		*(reinterpret_cast<uint32_t*>(dest)) = downsample_average_kernel_3(
				*(reinterpret_cast<uint64_t*>(row1)),
				*(reinterpret_cast<uint64_t*>(row2)));

		dest += c;
		row1 += 2 * lchannels;
		row2 += 2 * lchannels;
	}

	// last element
	a = downsample_average_kernel_3(
			*(reinterpret_cast<uint64_t*>(row1)),
			*(reinterpret_cast<uint64_t*>(row2)));
	// if dest == input, this write is fine.  else dest may have 1 byte written out of bound.
	memcpy(dest, reinterpret_cast<uint8_t*>(&a), c);


	// recurse
	if (downSamplingFactor > 2) {
		downsample_average_bytes_2pass_3(output, channels, input, lw >> 1, lh >> 1, lchannels, downSamplingFactor >> 1);
	} // else exactly 2, then done.
}



void downsample_nearest_1(uint32_t* output, const size_t w, const size_t h,
                          uint32_t* input, const size_t lw, const size_t lh,
                          const uint32_t downSamplingFactor) {

          // centered direct down sample loop.  nearest neighbor only
      uint64_t row, col;
      uint32_t *dest = output;
      uint32_t *src = input;
      uint32_t row_offset = downSamplingFactor >> 1,
    		  col_offset = downSamplingFactor >> 1;
      for (row = 0; row < h; ++row) {
        // condense the pixels to the first w positions into dest directly
        src = input + lw * row_offset;

        col_offset = downSamplingFactor / 2.0;
        for (col = 0; col < w; ++col) {
          *dest++ = *(src + col_offset);   // copy pixel from src to dest.
          col_offset += downSamplingFactor;   // accumulate src offset
        }
        row_offset += downSamplingFactor;
      }

}



void downsample_nearest_bytes(uint8_t* output, const size_t w, const size_t h, const int channels,
                          uint8_t* input, const size_t lw, const size_t lh, const int lchannels,
                          const uint32_t downSamplingFactor) {

          // centered direct down sample loop.  nearest neighbor only
      uint64_t row, col;
      uint8_t *dest = output;
      uint8_t *src = input;

      size_t rowsize = lchannels * lw;

      uint32_t row_offset = downSamplingFactor >> 1,
    		  col_offset;
      for (row = 0; row < h; ++row) {
        // condense the pixels to the first w positions into dest directly
        src = input + rowsize * row_offset;

        col_offset = downSamplingFactor >> 1;
        for (col = 0; col < w; ++col) {
          //*dest++ = *(src + channels * lround(col_offset));   // copy pixel from src to dest.
          memcpy(dest, src + lchannels * col_offset, channels);
          col_offset += downSamplingFactor;   // accumulate src offset
          dest += channels;
        }
        row_offset += downSamplingFactor;
      }

}


void downsample_nearest_2(uint32_t* output, const size_t w, const size_t h,
                          uint32_t* input, const size_t lw, const size_t lh,
                          const uint32_t downSamplingFactor) {

        // direct down sample loop.  nearest neighbor only
    uint64_t row, col;
    uint32_t *dest = output;
    uint32_t *src = input;
    uint32_t row_offset = 0, col_offset = 0;
    for (row = 0; row < h; ++row) {
      // condense the pixels to the first w positions into dest directly
      src = input + lw * row_offset;

      col_offset = 0.0;
      for (col = 0; col < w; ++col) {
        *dest++ = *(src + col_offset);   // copy pixel from src to dest.
        col_offset += downSamplingFactor;   // accumulate src offset
      }
      row_offset += downSamplingFactor;
    }
}

void downsample_nearest_3(uint32_t* output, const size_t w, const size_t h,
                          uint32_t* input, const size_t lw, const size_t lh,
                          const uint32_t downSamplingFactor) {


        // incremental down sample loop.  nearest neighbor only
    uint64_t row, col;
    uint32_t *dest = output;
    uint32_t *src = input;
    uint32_t row_offset = 0, col_offset = 0;
    for (row = 0; row < h; ++row) {
      // condense the pixels to the first w positions into dest directly
      src = input + lw * row_offset;

      // skip first.
      col_offset = downSamplingFactor;
      // do the rest of the line.
      for (col = 1; col < w; ++col) {
        *(src+col) = *(src + col_offset);   // copy pixel from src to dest.
        col_offset += downSamplingFactor;   // accumulate src offset
      }
      memcpy(dest, src, w * sizeof(uint32_t));
      row_offset += downSamplingFactor;
      dest += w;
    }
}

void downsample_nearest_4(uint32_t* output, const size_t w, const size_t h,
                          uint32_t* input, const size_t lw, const size_t lh,
                          const uint32_t downSamplingFactor) {


      // original down sample loop with slight cleanup.  nearest neighbor only
      uint64_t row, col;
      uint32_t *dest, *src, *cdest, *csrc;
      for (row = 0; row < h; ++row) {
          dest = output + row * w;  // destination row starting position
          // src row starting position
          src = input +  lw * row * downSamplingFactor;

          // modify the src row
          cdest = src;
          csrc = src;
          // condense the pixels to the first w positions.
          for (col = 1; col < w; ++col) {
              *(cdest + col) = *(csrc + col * downSamplingFactor);
          }
          // then copy to dest buffer.
          memcpy(dest, src, w * sizeof(uint32_t));
      }
}

void read_file(uint8_t* data, char* const filename, const size_t size_to_read) {
  // open file
  int fdin;
  struct stat statbuf;
  void *src;

  if ((fdin = open (filename, O_RDONLY)) < 0) {
    fprintf(stderr, "can't open %s for reading", filename);
    exit(1);
  }

  /* find size of input file */
  if (fstat (fdin,&statbuf) < 0) {
    fprintf(stderr, "fstat error");
    exit(1);
  }

  if (statbuf.st_size < size_to_read) {
    printf("ERROR: insufficient number of bytes to read");
    exit(1);
  }


  /* mmap the input file */
  if ((src = mmap (0, size_to_read, PROT_READ, MAP_SHARED, fdin, 0)) == (caddr_t) -1) {
    fprintf(stderr, "mmap error for input");
    exit(1);
  }

  // copy in the file.
  memcpy(data, src, size_to_read);
  // unmap the image
  munmap(src, size_to_read);
  close(fdin);

}

void write_data(char* const outfile, uint8_t* outdata, const size_t outsize) {
  int fdout;
  struct stat statbuf;
  void *dst;


  /* open/create the output file */
  if ((fdout = open (outfile, O_RDWR | O_CREAT | O_TRUNC,
                     S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP)) < 0) {
    fprintf(stderr, "can't create %s for writing", outfile);
    exit(1);
  }

  /* go to the location corresponding to the last byte */
  if (lseek (fdout, outsize - 1, SEEK_SET) == -1) {
    fprintf(stderr, "lseek error");
    exit(1);
  }

  /* write a dummy byte at the last location */
  if (write (fdout, "", 1) != 1) {
    fprintf(stderr, "write error");
    exit(1);
  }


  /* mmap the output file */
  if ((dst = mmap (0, outsize, PROT_READ | PROT_WRITE, MAP_SHARED, fdout, 0)) == (caddr_t) -1) {
    fprintf(stderr, "mmap error for output");
    exit(1);
  }


  /* this copies the input file to the output file */
  memcpy(dst, outdata, outsize);

  // cleanup
  munmap(dst, outsize);
  close(fdout);

}


int main(int argc, char** argv) {

  struct timeval  tv1, tv2;
  double duration;


  // input image conversion: http://www.imagemagick.org/Usage/formats/#rgb.  -alpha on to add/preserve alpha channel
  // convert simple.jpg -depth 8 -alpha on simple.rgba
  // convert -depth 8 -size 3840x2160 simple.rgba simple.png


  char* filename;
  size_t w;
  size_t h;
  int channels;

  if (argc > 3) {
    filename = argv[1];
    w = atoi(argv[2]);
    h = atoi(argv[3]);
    channels = 4;  // hardcoded, only support rgba
  } else {
    fprintf(stderr, "Usage: %s rawimage w h channels\n use convert {input} -size {w}x{h} rgb:{output} to generate\n", argv[0]);
  }


  // allocate storage
  int alignment = 32;
  size_t imagesize = w * h * channels * sizeof(uint8_t);
  uint8_t* data = (uint8_t*)aligned_alloc(32, imagesize);
  uint8_t* data_const = (uint8_t*)aligned_alloc(32, imagesize);

  // allocate the output
  uint8_t* outdata = (uint8_t*)aligned_alloc(32, imagesize);


  read_file(data_const, filename, imagesize);
  memcpy(data, data_const, imagesize);



  // format:  {inputfile}.{w}x{h}x{c}.{algo}.rgba, so 20 additional chars.
  int filename_len = strlen(argv[1]);
  char* outfile = (char*)malloc(filename_len + 20);
  // use sprintf to construct this.

  int max_downsample = 10;  // checking all power of 2 only.
  size_t outW, outH;
  size_t outsize;


  for (int i = 2; i < max_downsample; i <<= 1) {

    // compute the output dimensions
    outW = w / i;
    outH = h / i;
    outsize = outW * outH * channels;


//    //===  algorithms 4
//
//    // clear the output
//    memcpy(data, data_const, imagesize);
//
//    memset(outdata, 0, imagesize);
//    // set up the file name
//    memset(outfile, 0, filename_len + 20);
//    sprintf(outfile, "%s.%lux%lux%d.%d.rgba", filename, outW, outH, channels, 4);
//    // performs the downsamplings.
//
//    gettimeofday(&tv1, NULL);
//
//    downsample_nearest_4(reinterpret_cast<uint32_t*>(outdata), outW, outH,
//                         reinterpret_cast<uint32_t*>(data), w, h, i);
//    gettimeofday(&tv2, NULL);
//
//    duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
//             (double) (tv2.tv_sec - tv1.tv_sec);
//
//    write_data(outfile, outdata, outsize);
//    printf("Downsample factor %d for file %s using algo %d: time %fs\n", i, filename, 4, duration);
//
//    //===  algorithms 3
//
//    // clear the output
//    memcpy(data, data_const, imagesize);
//
//    memset(outdata, 0, imagesize);
//    // set up the file name
//    memset(outfile, 0, filename_len + 20);
//    sprintf(outfile, "%s.%lux%lux%d.%d.rgba", filename, outW, outH, channels, 3);
//    gettimeofday(&tv1, NULL);
//
//    // performs the downsamplings.
//    downsample_nearest_3(reinterpret_cast<uint32_t*>(outdata), outW, outH,
//                         reinterpret_cast<uint32_t*>(data), w, h, i);
//    gettimeofday(&tv2, NULL);
//
//    duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
//             (double) (tv2.tv_sec - tv1.tv_sec);
//
//    write_data(outfile, outdata, outsize);
//    printf("Downsample factor %d for file %s using algo %d: time %fs\n", i, filename, 3, duration);
//
//
//    //===  algorithms 2
//
//    // clear the output
//    memcpy(data, data_const, imagesize);
//
//    memset(outdata, 0, imagesize);
//    // set up the file name
//    memset(outfile, 0, filename_len + 20);
//    sprintf(outfile, "%s.%lux%lux%d.%d.rgba", filename, outW, outH, channels, 2);
//    gettimeofday(&tv1, NULL);
//
//    // performs the downsamplings.
//    downsample_nearest_2(reinterpret_cast<uint32_t*>(outdata), outW, outH,
//                         reinterpret_cast<uint32_t*>(data), w, h, i);
//    gettimeofday(&tv2, NULL);
//
//    duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
//             (double) (tv2.tv_sec - tv1.tv_sec);
//    write_data(outfile, outdata, outsize);
//    printf("Downsample factor %d for file %s using algo %d: time %fs\n", i, filename, 2, duration);
//
//
//
//    //===  algorithms 1
//
//    // clear the output
//    memcpy(data, data_const, imagesize);
//
//    memset(outdata, 0, imagesize);
//    // set up the file name
//    memset(outfile, 0, filename_len + 20);
//    sprintf(outfile, "%s.%lux%lux%d.%d.rgba", filename, outW, outH, channels, 1);
//    gettimeofday(&tv1, NULL);
//
//    // performs the downsamplings.
//    downsample_nearest_1(reinterpret_cast<uint32_t*>(outdata), outW, outH,
//                         reinterpret_cast<uint32_t*>(data), w, h, i);
//    gettimeofday(&tv2, NULL);
//
//    duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
//             (double) (tv2.tv_sec - tv1.tv_sec);
//    write_data(outfile, outdata, outsize);
//    printf("Downsample factor %d for file %s using algo %d: time %fs\n", i, filename, 1, duration);
//
    //===  algorithms bt bytes

    // clear the output
    memcpy(data, data_const, imagesize);

    memset(outdata, 0, imagesize);
    // set up the file name
    memset(outfile, 0, filename_len + 20);
    sprintf(outfile, "%s.%lux%lux%d.%d.rgba", filename, outW, outH, channels, 0);
    gettimeofday(&tv1, NULL);

    // performs the downsamplings.
    downsample_nearest_bytes(outdata, outW, outH, channels,
                         	 data, w, h, channels, i);
    gettimeofday(&tv2, NULL);

    duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
             (double) (tv2.tv_sec - tv1.tv_sec);
    write_data(outfile, outdata, outsize);
    printf("Downsample 4->4 factor %d for file %s using algo %d: time %fs\n", i, filename, 0, duration);


    //===  algorithms bt bytes

    // clear the output
    memcpy(data, data_const, imagesize);

    memset(outdata, 0, imagesize);
    // set up the file name
    memset(outfile, 0, filename_len + 20);
    sprintf(outfile, "%s.%lux%lux%d.%d.rgb", filename, outW, outH, 3, 0);
    gettimeofday(&tv1, NULL);

    // performs the downsamplings.
    downsample_nearest_bytes(outdata, outW, outH, 3,
                         	 data, w, h, channels, i);
    gettimeofday(&tv2, NULL);

    duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
             (double) (tv2.tv_sec - tv1.tv_sec);
    write_data(outfile, outdata, outW * outH * 3);
    printf("Downsample 4->3 factor %d for file %s using algo %d: time %fs\n", i, filename, 0, duration);


    //===  algorithms bt bytes

    // clear the output and convert to 3 bytes
    for (int i = 0; i < w*h; ++i) {
    	memcpy(data + i * 3, data_const + i * channels, 3);
    }
    // convert to 3 bytes

    memset(outdata, 0, imagesize);
    // set up the file name
    memset(outfile, 0, filename_len + 21);
    sprintf(outfile, "%s.%lux%lux%d.%d.rgb", filename, outW, outH, 33, 0);
    gettimeofday(&tv1, NULL);

    // performs the downsamplings.
    downsample_nearest_bytes(outdata, outW, outH, 3,
                         	 data, w, h, 3, i);
    gettimeofday(&tv2, NULL);

    duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
             (double) (tv2.tv_sec - tv1.tv_sec);
    write_data(outfile, outdata, outW * outH * 3);
    printf("Downsample 3->3 factor %d for file %s using algo %d: time %fs\n", i, filename, 0, duration);


    //===  algorithms 10

    // clear the output
    memcpy(data, data_const, imagesize);

    memset(outdata, 0, imagesize);
    // set up the file name
    memset(outfile, 0, filename_len + 23);
    sprintf(outfile, "%s.%lux%lux%d.%s.rgba", filename, outW, outH, channels, "avg");

    gettimeofday(&tv1, NULL);
    // performs the downsamplings.
    downsample_average(reinterpret_cast<uint32_t*>(outdata),
                         reinterpret_cast<uint32_t*>(data), w, h, i);
    gettimeofday(&tv2, NULL);

    duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
             (double) (tv2.tv_sec - tv1.tv_sec);
    write_data(outfile, outdata, outsize);
    printf("Downsample factor %d for file %s using algo %s: time %fs\n", i, filename, "avg", duration);


    //===  algorithms 11 4->4

    // clear the output
    memcpy(data, data_const, imagesize);

    memset(outdata, 0, imagesize);
    // set up the file name
    memset(outfile, 0, filename_len + 25);
    sprintf(outfile, "%s.%lux%lux%d.%s.rgba", filename, outW, outH, channels, "avg44");

    gettimeofday(&tv1, NULL);
    // performs the downsamplings.
    downsample_average_bytes(outdata, 4,
                         data, w, h, 4, i);
    gettimeofday(&tv2, NULL);

    duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
             (double) (tv2.tv_sec - tv1.tv_sec);
    write_data(outfile, outdata, outsize);
    printf("Downsample 4->4 factor %d for file %s using algo %s: time %fs\n", i, filename, "avg", duration);

    //===  algorithms 10

    // clear the output
    memcpy(data, data_const, imagesize);

    memset(outdata, 0, imagesize);
    // set up the file name
    memset(outfile, 0, filename_len + 25);
    sprintf(outfile, "%s.%lux%lux%d.%s.rgb", filename, outW, outH, channels, "avg43");

    gettimeofday(&tv1, NULL);
    // performs the downsamplings.
    downsample_average_bytes(outdata, 3,
                         data, w, h, 4, i);
    gettimeofday(&tv2, NULL);

    duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
             (double) (tv2.tv_sec - tv1.tv_sec);
    write_data(outfile, outdata, outW * outH * 3);
    printf("Downsample 4->3 factor %d for file %s using algo %s: time %fs\n", i, filename, "avg", duration);


    //===  algorithms 10

    // clear the output
    // clear the output and convert to 3 bytes
    for (int i = 0; i < w*h; ++i) {
    	memcpy(data + i * 3, data_const + i * channels, 3);
    }

    memset(outdata, 0, imagesize);
    // set up the file name
    memset(outfile, 0, filename_len + 25);
    sprintf(outfile, "%s.%lux%lux%d.%s.rgb", filename, outW, outH, channels, "avg33");

    gettimeofday(&tv1, NULL);
    // performs the downsamplings.
    downsample_average_bytes(outdata, 3,
                         data, w, h, 3, i);
    gettimeofday(&tv2, NULL);

    duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
             (double) (tv2.tv_sec - tv1.tv_sec);
    write_data(outfile, outdata, outW * outH * 3);
    printf("Downsample 3->3 factor %d for file %s using algo %s: time %fs\n", i, filename, "avg", duration);


    //===  algorithms 11 4->4

    // clear the output
    memcpy(data, data_const, imagesize);

    memset(outdata, 0, imagesize);
    // set up the file name
    memset(outfile, 0, filename_len + 27);
    sprintf(outfile, "%s.%lux%lux%d.%s.rgba", filename, outW, outH, channels, "2pavg44");

    gettimeofday(&tv1, NULL);
    // performs the downsamplings.
    downsample_average_bytes_2pass(outdata, 4,
                         data, w, h, 4, i);
    gettimeofday(&tv2, NULL);

    duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
             (double) (tv2.tv_sec - tv1.tv_sec);
    write_data(outfile, outdata, outsize);
    printf("Downsample 4->4 factor %d for file %s using algo %s: time %fs\n", i, filename, "2pavg", duration);

    //===  algorithms 10

    // clear the output
    memcpy(data, data_const, imagesize);

    memset(outdata, 0, imagesize);
    // set up the file name
    memset(outfile, 0, filename_len + 27);
    sprintf(outfile, "%s.%lux%lux%d.%s.rgb", filename, outW, outH, channels, "2pavg43");

    gettimeofday(&tv1, NULL);
    // performs the downsamplings.
    downsample_average_bytes_2pass(outdata, 3,
                         data, w, h, 4, i);
    gettimeofday(&tv2, NULL);

    duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
             (double) (tv2.tv_sec - tv1.tv_sec);
    write_data(outfile, outdata, outW * outH * 3);
    printf("Downsample 4->3 factor %d for file %s using algo %s: time %fs\n", i, filename, "2pavg", duration);


    //===  algorithms 10

    // clear the output
    // clear the output and convert to 3 bytes
    for (int i = 0; i < w*h; ++i) {
    	memcpy(data + i * 3, data_const + i * channels, 3);
    }

    memset(outdata, 0, imagesize);
    // set up the file name
    memset(outfile, 0, filename_len + 27);
    sprintf(outfile, "%s.%lux%lux%d.%s.rgb", filename, outW, outH, channels, "2pavg33");

    gettimeofday(&tv1, NULL);
    // performs the downsamplings.
    downsample_average_bytes_2pass(outdata, 3,
                         data, w, h, 3, i);
    gettimeofday(&tv2, NULL);

    duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
             (double) (tv2.tv_sec - tv1.tv_sec);
    write_data(outfile, outdata, outW * outH * 3);
    printf("Downsample 3->3 factor %d for file %s using algo %s: time %fs\n", i, filename, "2pavg", duration);


    //===  algorithms 11 4->4

    // clear the output
    memcpy(data, data_const, imagesize);

    memset(outdata, 0, imagesize);
    // set up the file name
    memset(outfile, 0, filename_len + 27);
    sprintf(outfile, "%s.%lux%lux%d.%s.rgba", filename, outW, outH, channels, "wdavg44");

    gettimeofday(&tv1, NULL);
    // performs the downsamplings.
    downsample_average_bytes_2pass_4(outdata, 4,
                         data, w, h, 4, i);
    gettimeofday(&tv2, NULL);

    duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
             (double) (tv2.tv_sec - tv1.tv_sec);
    write_data(outfile, outdata, outsize);
    printf("Downsample 4->4 factor %d for file %s using algo %s: time %fs\n", i, filename, "wdavg", duration);


    //===  algorithms 10

    // clear the output
    // clear the output and convert to 3 bytes
    for (int i = 0; i < w*h; ++i) {
    	memcpy(data + i * 3, data_const + i * channels, 3);
    }

    memset(outdata, 0, imagesize);
    // set up the file name
    memset(outfile, 0, filename_len + 27);
    sprintf(outfile, "%s.%lux%lux%d.%s.rgb", filename, outW, outH, channels, "wdavg33");

    gettimeofday(&tv1, NULL);
    // performs the downsamplings.
    downsample_average_bytes_2pass_3(outdata, 3,
                         data, w, h, 3, i);
    gettimeofday(&tv2, NULL);

    duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
             (double) (tv2.tv_sec - tv1.tv_sec);
    write_data(outfile, outdata, outW * outH * 3);
    printf("Downsample 3->3 factor %d for file %s using algo %s: time %fs\n", i, filename, "wdavg", duration);

  }

  // cleanup
  free(data);
  free(data_const);
  free(outdata);
  free(outfile);
}
