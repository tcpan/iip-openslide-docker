/**
 * @file    test.cpp
 * @ingroup
 * @author  tpan
 * @brief
 * @details
 *
 * Copyright (c) 2014 Georgia Institute of Technology.  All Rights Reserved.
 *
 * TODO add License
 */


#include <cstdio>
#include <cstdlib>    // exit
#include <cmath>
#include <cstring>
#include <stdint.h>  // uintxx_t
#include <cassert>   //assert
#include <fcntl.h>   // open
#include <sys/stat.h>
#include <sys/types.h> // err_sys
#include <sys/mman.h> // mmap()
#include <unistd.h>  //lseek, write

#include <sys/time.h>

  // test different downsample codes

void downsample_average_1 (uint32_t* output, const size_t w, const size_t h,
                           uint32_t* input, const size_t lw, const size_t lh,
                              const double downSamplingFactor) {

  bool broken_do_not_use = false;
  assert(broken_do_not_use);

  // direct down sample loop.  area averaged
  uint64_t row, col, srcrow1, srcrow2 = 0, srccol1, srccol2 = 0;
  uint32_t *dest = output;
  uint32_t *src = input;
  double newval;
  int count;
  int i, j;
  double row_offset = 0.0, col_offset = 0.0;
  for (row = 0; row < h; ++row) {
    srcrow1 = srcrow2;
    row_offset += downSamplingFactor;
    srcrow2 = lround(row_offset);

    // condense the pixels to the first w positions into dest directly
    col_offset = 0.0;
    srccol2 = 0;
    for (col = 0; col < w; ++col) {
      srccol1 = srccol2;
      col_offset += downSamplingFactor;
      srccol2 = lround(col_offset);

      newval = 0.0;
      count = 0;
      src = input + lw * srcrow1 + srccol1;
      for (j = srcrow1; j < srcrow2; ++j) {

        for (i = srccol1; i < srccol2; ++i) {
          // TODO:  averaging.
          newval += *(src + i);
          ++count;      // > 0.  downSamplingFactor > 1 =>  srccol2 >= srccol1 + 1
        }
        src += lw;
      }


      *dest = newval / count;   // copy pixel from src to dest.
      ++dest;
    }
  }

}


void downsample_nearest_1(uint32_t* output, const size_t w, const size_t h,
                          uint32_t* input, const size_t lw, const size_t lh,
                          const double downSamplingFactor) {

          // centered direct down sample loop.  nearest neighbor only
      uint64_t row, col;
      uint32_t *dest = output;
      uint32_t *src = input;
      double row_offset = downSamplingFactor / 2.0, col_offset = downSamplingFactor / 2.0;
      for (row = 0; row < h; ++row) {
        // condense the pixels to the first w positions into dest directly
        src = input + lw * lround(row_offset);

        col_offset = downSamplingFactor / 2.0;
        for (col = 0; col < w; ++col) {
          *dest++ = *(src + lround(col_offset));   // copy pixel from src to dest.
          col_offset += downSamplingFactor;   // accumulate src offset
        }
        row_offset += downSamplingFactor;
      }

}



void downsample_nearest_bytes(uint8_t* output, const size_t w, const size_t h, const int channels,
                          uint8_t* input, const size_t lw, const size_t lh,
                          const double downSamplingFactor) {

          // centered direct down sample loop.  nearest neighbor only
      uint64_t row, col;
      uint8_t *dest = output;
      uint8_t *src = input;

      size_t rowsize = channels * lw;

      double row_offset = downSamplingFactor / 2.0,
    		  col_offset;
      for (row = 0; row < h; ++row) {
        // condense the pixels to the first w positions into dest directly
        src = input + rowsize * lround(row_offset);

        col_offset = downSamplingFactor / 2.0;
        for (col = 0; col < w; ++col) {
          //*dest++ = *(src + channels * lround(col_offset));   // copy pixel from src to dest.
          memcpy(dest, src + channels * lround(col_offset), channels);
          col_offset += downSamplingFactor;   // accumulate src offset
          dest += channels;
        }
        row_offset += downSamplingFactor;
      }

}


void downsample_nearest_2(uint32_t* output, const size_t w, const size_t h,
                          uint32_t* input, const size_t lw, const size_t lh,
                          const double downSamplingFactor) {

        // direct down sample loop.  nearest neighbor only
    uint64_t row, col;
    uint32_t *dest = output;
    uint32_t *src = input;
    double row_offset = 0.0, col_offset = 0.0;
    for (row = 0; row < h; ++row) {
      // condense the pixels to the first w positions into dest directly
      src = input + lw * lround(row_offset);

      col_offset = 0.0;
      for (col = 0; col < w; ++col) {
        *dest++ = *(src + lround(col_offset));   // copy pixel from src to dest.
        col_offset += downSamplingFactor;   // accumulate src offset
      }
      row_offset += downSamplingFactor;
    }
}

void downsample_nearest_3(uint32_t* output, const size_t w, const size_t h,
                          uint32_t* input, const size_t lw, const size_t lh,
                          const double downSamplingFactor) {


        // incremental down sample loop.  nearest neighbor only
    uint64_t row, col;
    uint32_t *dest = output;
    uint32_t *src = input;
    double row_offset = 0.0, col_offset = 0.0;
    for (row = 0; row < h; ++row) {
      // condense the pixels to the first w positions into dest directly
      src = input + lw * lround(row_offset);

      // skip first.
      col_offset = downSamplingFactor;
      // do the rest of the line.
      for (col = 1; col < w; ++col) {
        *(src+col) = *(src + lround(col_offset));   // copy pixel from src to dest.
        col_offset += downSamplingFactor;   // accumulate src offset
      }
      memcpy(dest, src, w * sizeof(uint32_t));
      row_offset += downSamplingFactor;
      dest += w;
    }
}

void downsample_nearest_4(uint32_t* output, const size_t w, const size_t h,
                          uint32_t* input, const size_t lw, const size_t lh,
                          const double downSamplingFactor) {


      // original down sample loop with slight cleanup.  nearest neighbor only
      uint64_t row, col;
      uint32_t *dest, *src, *cdest, *csrc;
      for (row = 0; row < h; ++row) {
          dest = output + row * w;  // destination row starting position
          // src row starting position
          src = input +  lw * lround(static_cast<double>(row) * downSamplingFactor);

          // modify the src row
          cdest = src;
          csrc = src;
          // condense the pixels to the first w positions.
          for (col = 1; col < w; ++col) {
              *(cdest + col) = *(csrc + lround(static_cast<double>(col) * downSamplingFactor));
          }
          // then copy to dest buffer.
          memcpy(dest, src, w * sizeof(uint32_t));
      }
}

void read_file(uint8_t* data, char* const filename, const size_t size_to_read) {
  // open file
  int fdin;
  struct stat statbuf;
  void *src;

  if ((fdin = open (filename, O_RDONLY)) < 0) {
    fprintf(stderr, "can't open %s for reading", filename);
    exit(1);
  }

  /* find size of input file */
  if (fstat (fdin,&statbuf) < 0) {
    fprintf(stderr, "fstat error");
    exit(1);
  }

  if (statbuf.st_size < size_to_read) {
    printf("ERROR: insufficient number of bytes to read");
    exit(1);
  }


  /* mmap the input file */
  if ((src = mmap (0, size_to_read, PROT_READ, MAP_SHARED, fdin, 0)) == (caddr_t) -1) {
    fprintf(stderr, "mmap error for input");
    exit(1);
  }

  // copy in the file.
  memcpy(data, src, size_to_read);
  // unmap the image
  munmap(src, size_to_read);
  close(fdin);

}

void write_data(char* const outfile, uint8_t* outdata, const size_t outsize) {
  int fdout;
  struct stat statbuf;
  void *dst;


  /* open/create the output file */
  if ((fdout = open (outfile, O_RDWR | O_CREAT | O_TRUNC,
                     S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP)) < 0) {
    fprintf(stderr, "can't create %s for writing", outfile);
    exit(1);
  }

  /* go to the location corresponding to the last byte */
  if (lseek (fdout, outsize - 1, SEEK_SET) == -1) {
    fprintf(stderr, "lseek error");
    exit(1);
  }

  /* write a dummy byte at the last location */
  if (write (fdout, "", 1) != 1) {
    fprintf(stderr, "write error");
    exit(1);
  }


  /* mmap the output file */
  if ((dst = mmap (0, outsize, PROT_READ | PROT_WRITE, MAP_SHARED, fdout, 0)) == (caddr_t) -1) {
    fprintf(stderr, "mmap error for output");
    exit(1);
  }


  /* this copies the input file to the output file */
  memcpy(dst, outdata, outsize);

  // cleanup
  munmap(dst, outsize);
  close(fdout);

}


int main(int argc, char** argv) {

  struct timeval  tv1, tv2;
  double duration;


  // input image conversion: http://www.imagemagick.org/Usage/formats/#rgb.  -alpha on to add/preserve alpha channel
  // convert simple.jpg -depth 8 -alpha on simple.rgba
  // convert -depth 8 -size 3840x2160 simple.rgba simple.png


  char* filename;
  size_t w;
  size_t h;
  int channels;

  if (argc > 3) {
    filename = argv[1];
    w = atoi(argv[2]);
    h = atoi(argv[3]);
    channels = 4;  // hardcoded, only support rgba
  } else {
    fprintf(stderr, "Usage: %s rawimage w h channels\n use convert {input} -size {w}x{h} rgb:{output} to generate\n", argv[0]);
  }


  // allocate storage
  int alignment = 32;
  size_t imagesize = w * h * channels * sizeof(uint8_t);
  uint8_t* data = (uint8_t*)aligned_alloc(32, imagesize);
  uint8_t* data_const = (uint8_t*)aligned_alloc(32, imagesize);

  // allocate the output
  uint8_t* outdata = (uint8_t*)aligned_alloc(32, imagesize);


  read_file(data_const, filename, imagesize);
  memcpy(data, data_const, imagesize);



  // format:  {inputfile}.{w}x{h}x{c}.{algo}.rgba, so 20 additional chars.
  int filename_len = strlen(argv[1]);
  char* outfile = (char*)malloc(filename_len + 20);
  // use sprintf to construct this.

  int max_downsample = (w + 255) / 256;  // checking all non power of 2 downsamples too.
  size_t outW, outH;
  size_t outsize;


  for (int i = 2; i < max_downsample; ++i) {

    // compute the output dimensions
    outW = w / i;
    outH = h / i;
    outsize = outW * outH * channels;


    //===  algorithms 4

    // clear the output
    memcpy(data, data_const, imagesize);

    memset(outdata, 0, imagesize);
    // set up the file name
    memset(outfile, 0, filename_len + 20);
    sprintf(outfile, "%s.%lux%lux%d.%d.rgba", filename, outW, outH, channels, 4);
    // performs the downsamplings.

    gettimeofday(&tv1, NULL);

    downsample_nearest_4(reinterpret_cast<uint32_t*>(outdata), outW, outH,
                         reinterpret_cast<uint32_t*>(data), w, h, i);
    gettimeofday(&tv2, NULL);

    duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
             (double) (tv2.tv_sec - tv1.tv_sec);

    write_data(outfile, outdata, outsize);
    printf("Downsample factor %d for file %s using algo %d: time %fs\n", i, filename, 4, duration);

    //===  algorithms 3

    // clear the output
    memcpy(data, data_const, imagesize);

    memset(outdata, 0, imagesize);
    // set up the file name
    memset(outfile, 0, filename_len + 20);
    sprintf(outfile, "%s.%lux%lux%d.%d.rgba", filename, outW, outH, channels, 3);
    gettimeofday(&tv1, NULL);

    // performs the downsamplings.
    downsample_nearest_3(reinterpret_cast<uint32_t*>(outdata), outW, outH,
                         reinterpret_cast<uint32_t*>(data), w, h, i);
    gettimeofday(&tv2, NULL);

    duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
             (double) (tv2.tv_sec - tv1.tv_sec);

    write_data(outfile, outdata, outsize);
    printf("Downsample factor %d for file %s using algo %d: time %fs\n", i, filename, 3, duration);


    //===  algorithms 2

    // clear the output
    memcpy(data, data_const, imagesize);

    memset(outdata, 0, imagesize);
    // set up the file name
    memset(outfile, 0, filename_len + 20);
    sprintf(outfile, "%s.%lux%lux%d.%d.rgba", filename, outW, outH, channels, 2);
    gettimeofday(&tv1, NULL);

    // performs the downsamplings.
    downsample_nearest_2(reinterpret_cast<uint32_t*>(outdata), outW, outH,
                         reinterpret_cast<uint32_t*>(data), w, h, i);
    gettimeofday(&tv2, NULL);

    duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
             (double) (tv2.tv_sec - tv1.tv_sec);
    write_data(outfile, outdata, outsize);
    printf("Downsample factor %d for file %s using algo %d: time %fs\n", i, filename, 2, duration);



    //===  algorithms 1

    // clear the output
    memcpy(data, data_const, imagesize);

    memset(outdata, 0, imagesize);
    // set up the file name
    memset(outfile, 0, filename_len + 20);
    sprintf(outfile, "%s.%lux%lux%d.%d.rgba", filename, outW, outH, channels, 1);
    gettimeofday(&tv1, NULL);

    // performs the downsamplings.
    downsample_nearest_1(reinterpret_cast<uint32_t*>(outdata), outW, outH,
                         reinterpret_cast<uint32_t*>(data), w, h, i);
    gettimeofday(&tv2, NULL);

    duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
             (double) (tv2.tv_sec - tv1.tv_sec);
    write_data(outfile, outdata, outsize);
    printf("Downsample factor %d for file %s using algo %d: time %fs\n", i, filename, 1, duration);

    //===  algorithms bt bytes

    // clear the output
    memcpy(data, data_const, imagesize);

    memset(outdata, 0, imagesize);
    // set up the file name
    memset(outfile, 0, filename_len + 20);
    sprintf(outfile, "%s.%lux%lux%d.%d.rgba", filename, outW, outH, channels, 0);
    gettimeofday(&tv1, NULL);

    // performs the downsamplings.
    downsample_nearest_bytes(outdata, outW, outH, 4,
                         	 data, w, h, i);
    gettimeofday(&tv2, NULL);

    duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
             (double) (tv2.tv_sec - tv1.tv_sec);
    write_data(outfile, outdata, outsize);
    printf("Downsample factor %d for file %s using algo %d: time %fs\n", i, filename, 0, duration);


//    //===  algorithms 0
//
//    // clear the output
//    memcpy(data, data_const, imagesize);
//
//    memset(outdata, 0, imagesize);
//    // set up the file name
//    memset(outfile, 0, filename_len + 20);
//    sprintf(outfile, "%s.%lux%lux%d.%d.rgba", filename, outW, outH, channels, 0);
//
//    gettimeofday(&tv1, NULL);
//    // performs the downsamplings.
//    downsample_average_1(reinterpret_cast<uint32_t*>(outdata), outW, outH,
//                         reinterpret_cast<uint32_t*>(data), w, h, i);
//    gettimeofday(&tv2, NULL);
//
//    duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
//             (double) (tv2.tv_sec - tv1.tv_sec);
//    write_data(outfile, outdata, outsize);
//    printf("Downsample factor %d for file %s using algo %d: time %fs\n", i, filename, 0, duration);


  }

  // cleanup
  free(data);
  free(data_const);
  free(outdata);
  free(outfile);
}
