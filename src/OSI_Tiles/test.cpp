/**
 * @file    test.cpp
 * @ingroup
 * @author  tpan
 * @brief
 * @details
 *
 * Copyright (c) 2014 Georgia Institute of Technology.  All Rights Reserved.
 *
 * TODO add License
 */


extern "C" {
#include "openslide.h"
#include "openslide-features.h"
//#include "openslide-private.h"
//#include "openslide-decode-tiff.h"
//#include <tiffio.h>
//#include <glib.h>
}

//#include <cstdio>
#include <stdlib.h>    // exit, atoi
#include <math.h>	// log2
#include <string.h>     // memcmp
//#include <stdint.h>  // uintxx_t
//#include <cassert>   //assert
//#include <fcntl.h>   // open
//#include <sys/stat.h>
//#include <sys/types.h> // err_sys
//#include <sys/mman.h> // mmap()
//#include <unistd.h>  //lseek, write

#include <sys/time.h>   // get_time_of_day

#include <iostream>    //cout cerr endl
using namespace std;

//// copied from openslide-vendor-aperio
//extern "C" struct aperio_ops_data {
//  struct _openslide_tiffcache *tc;
//};
//
//extern "C" struct level {
//  struct _openslide_level base;
//  struct _openslide_tiff_level tiffl;
//  struct _openslide_grid *grid;
//  struct level *prev;
//  GHashTable *missing_tiles;
//  uint16_t compression;
//};
//
//static bool decode_tile(struct level *l,
//                        TIFF *tiff,
//                        uint32_t *dest,
//                        int64_t tile_col, int64_t tile_row,
//                        GError **err) {
//  struct _openslide_tiff_level *tiffl = &l->tiffl;
//
//  // check for missing tile
//  int64_t tile_no = tile_row * tiffl->tiles_across + tile_col;
//  if (g_hash_table_lookup_extended(l->missing_tiles, &tile_no, NULL, NULL)) {
//    //g_debug("missing tile in level %p: (%"PRId64", %"PRId64")", (void *) l, tile_col, tile_row);
//    return render_missing_tile(l, tiff, dest,
//                               tile_col, tile_row, err);
//  }
//
//  // select color space
//  enum _openslide_jp2k_colorspace space;
//  switch (l->compression) {
//  case APERIO_COMPRESSION_JP2K_YCBCR:
//    space = OPENSLIDE_JP2K_YCBCR;
//    break;
//  case APERIO_COMPRESSION_JP2K_RGB:
//    space = OPENSLIDE_JP2K_RGB;
//    break;
//  default:
//    // not for us? fallback
//    return _openslide_tiff_read_tile(tiffl, tiff, dest,
//                                     tile_col, tile_row,
//                                     err);
//  }
//
//  // read raw tile
//  void *buf;
//  int32_t buflen;
//  if (!_openslide_tiff_read_tile_data(tiffl, tiff,
//                                      &buf, &buflen,
//                                      tile_col, tile_row,
//                                      err)) {
//    return false;  // ok, haven't allocated anything yet
//  }
//
//  // decompress
//  bool success = _openslide_jp2k_decode_buffer(dest,
//                                               tiffl->tile_w, tiffl->tile_h,
//                                               buf, buflen,
//                                               space,
//                                               err);
//
//  // clean up
//  g_free(buf);
//
//  return success;
//}


bool areSame(const uint32_t* data1, const uint32_t* data2, int pix_count) {
	return memcmp(data1, data2, pix_count * sizeof(uint32_t)) == 0;
}


bool get_tile_direct(openslide_t *osr, int res, long tx, long ty, uint32_t* data) {
//	//  start with svs image.
//	GError *err = NULL;
//	  aperio_ops_data *d = reinterpret_cast<aperio_ops_data*>(osr->data);
//	  TIFF *tiff = _openslide_tiffcache_get(d->tc, &err);
//	  if (tiff == NULL) {
//	    return false;
//	  }
//	  level *l = (level *) osr->levels[res];
//
//	bool success = decode_tile(l, tiff, data, tx, ty, &err);
//	_openslide_tiffcache_put(d->tc, tiff);
//
//	return success;
}



int main(int argc, char** argv) {

  struct timeval  tv1, tv2;
  double duration;




  char* filename;
  int tx;
  int ty;
  int res;

  if (argc > 4) {
    filename = argv[1];
    tx = atoi(argv[2]);
    ty = atoi(argv[3]);
    res = atoi(argv[4]);
  } else {
    cerr << "Usage: " << argv[0] << " openslideImageFile tilex tiley resolution level" << endl;
    exit(1);
  }


  // openimage
  openslide_t* osr = openslide_open(filename);
  const char* error = openslide_get_error(osr);

  if (error) {
	  cerr << "ERROR: encountered error opening \"" << filename << "\", msg \"" << error << "\"" << endl;
	  exit(1);
  }
  if (!osr) {
	  cerr << "ERROR: unable to open \"" << filename << "\", no error msg" << endl;
	  exit(1);
  }

  // get number of resolutions and bound check input param
  int32_t openslide_levels = openslide_get_level_count(osr);
	error = openslide_get_error(osr);
	if (error) {
	  cerr << "ERROR: encountered error: " << error << " while getting level count. " << endl;
	  exit(1);
	}
	if (res >= openslide_levels) {
		cerr << "ERROR: specified resolution: " << res << " is larger than max resolution " << (openslide_levels - 1) << endl;
		exit(1);
	}
	cout << "number of resolutions: " << openslide_levels << " requesting " << res << endl;

  // get tile dimension at resolution
	const char* prop_val;
	int tw, th;
	prop_val = openslide_get_property_value(osr, "openslide.level[0].tile-width");
	error = openslide_get_error(osr);
	if (error) {
	  cerr << "ERROR: encountered error: " << error << " while getting property." << endl;
	  exit(1);
	}
    if (prop_val)  tw = atoi(prop_val);
    else tw = 256;

    prop_val = openslide_get_property_value(osr, "openslide.level[0].tile-height");
	error = openslide_get_error(osr);
	if (error) {
      cerr << "ERROR: encountered error: " << error << " while getting property." << endl;
      exit(1);
	}
    if (prop_val)  th = atoi(prop_val);
    else th = 256;

    cout << "tile dim:  w x h " << tw << "x" << th << endl;


    // get image size at resolution and compute number of tiles
	long ww, hh;
	openslide_get_level_dimensions(osr, res, &ww, &hh);
	error = openslide_get_error(osr);
	if (error) {
	  cerr << "ERROR: encountered error: " << error << " while getting level dims. " << endl;
	  exit(1);
	}
    cout << "image level dim:  res: w x h = " << res << ":" << ww << "x" << hh << endl;

    long ww0, hh0;
	openslide_get_level_dimensions(osr, 0, &ww0, &hh0);
	error = openslide_get_error(osr);
	if (error) {
	  cerr << "ERROR: encountered error: " << error << " while getting level dims. " << endl;
	  exit(1);
	}
    cout << "image level dim:  res: w x h = 0:" << ww0 << "x" << hh0 << endl;

    int ntx, nty;
    ntx = (ww + tw -1) / tw;
    nty = (hh + th -1) / th;


  // bound check the input params
    if (tx >= ntx) {
    	cerr << "ERROR: specified tile x " << tx << ", available " << ntx << endl;
    	exit(1);
    }
    if (ty >= nty) {
    	cerr << "ERROR: specified tile y " << ty << ", available " << nty << endl;
    	exit(1);
    }

    // compute the edge tile sizes
    int lasttw, lastth;
    lasttw = ww % tw;
    lastth = hh % th;
    cout << "image level tile count:  w x h " << ntx << "x" << nty << endl;
    cout << "image level last col width " << lasttw << " last row height " << lastth << endl;




  //==== use public api to read a region, time it.  compute the x and y in level 0 coordinate
    uint32_t* data1 = new uint32_t[tw * th];

    gettimeofday(&tv1, NULL);

    // compute the integer log2 of downsample factor
    uint32_t mag = log2(ww0/ww);
    cout << "image level " << res << " downsample: " << (ww0/ww) << " log2 = " << mag << endl;

    // compute the actual tile size

    int ttw = tw;
    if (tx == ntx - 1) ttw = lasttw;
    int tth = th;
    if (ty == nty - 1) tth = lastth;

    size_t tx0 = (tx * tw) << mag;  // same as multiply by z power of 2
    size_t ty0 = (ty * th) << mag;

    cerr << "Reading region at res " << res << ":" << tx << "x" << ty << " level 0 pos=" << tx0 << "x" << ty0 << " dim " << ttw << "x" << tth << " with OpenSlide" << endl;
    openslide_read_region(osr, data1, tx0, ty0, res, ttw, tth);
      error = openslide_get_error(osr);
      if (error) {
        cerr << "ERROR: encountered error: " << error << " while reading region" << endl;
        exit(1);
      }

    gettimeofday(&tv2, NULL);

    duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
             (double) (tv2.tv_sec - tv1.tv_sec);
    cout << "openslide_read_region() took << "<< duration << " ms" << endl;

  // use private api to read a tile, time it.
      uint32_t* data2 = new uint32_t[tw * th];

      gettimeofday(&tv1, NULL);

      get_tile_direct(osr, res, tx, ty, data2);

      gettimeofday(&tv2, NULL);

      duration = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
               (double) (tv2.tv_sec - tv1.tv_sec);
      cout << "get_tile_direct() took << "<< duration << " ms" << endl;


// compare
      bool same = areSame(data1, data2, ttw*tth);
      cout << "read data are " << (same ? "same" : "different") << endl;

  // close image
      cout << "done. cleaning up." << endl;
  if (osr != NULL) {
    openslide_close(osr);
    osr = NULL;
  }

  // clean up
  delete [] data1;
  delete [] data2;


}
